# Host application upgrade notes

Version 5.4.0 has replaced analytics with atlassian-profiling to emit JMX metrics

## Jmx metrics

No effort is required to emit JMX metrics, they will be emitted by default. We do however need to ensure
that we have the correct version of `atlassian-profiling` provided for the metrics to work. At the time of of writing 
this guide the recommended (minimum) version is `3.7.0`

