package com.atlassian.plugin.web.descriptors;

import com.atlassian.plugin.web.ContextProvider;
import com.atlassian.plugin.web.model.WebPanel;

/**
 * <p>
 * The web panel module declares a single web panel in atlassian-plugin.xml. Its
 * XML element contains a location string that should match existing locations
 * in the host application where web panels can be embedded.
 * </p>
 * <p>
 * The descriptor specifies a resource or class that renders HTML given a context map,
 * and may specify a {@link ContextProvider} that augments the context with custom
 * properties.
 * </p>
 *
 * @since 2.6.0
 * @deprecated since 6.1.0 use {@link com.atlassian.plugin.web.api.descriptors.WebPanelModuleDescriptor}. Will be
 * removed in the next major version.
 */
public interface WebPanelModuleDescriptor
        extends WebFragmentModuleDescriptor<WebPanel>,
                WeightedDescriptor,
                com.atlassian.plugin.web.api.descriptors.WebPanelModuleDescriptor<WebPanel> {}
