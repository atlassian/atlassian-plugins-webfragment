package com.atlassian.plugin.web.descriptors;

import com.atlassian.plugin.web.api.WebSection;
import com.atlassian.plugin.web.model.WebIcon;
import com.atlassian.plugin.web.model.WebLink;

/**
 * A web-item plugin adds extra links to a particular {@link WebSection section}.
 *
 * @see WebSectionModuleDescriptor
 * @deprecated since 6.1.0 use {@link com.atlassian.plugin.web.api.descriptors.WebItemModuleDescriptor}
 */
public interface WebItemModuleDescriptor
        extends WebFragmentModuleDescriptor<Void>, com.atlassian.plugin.web.api.descriptors.WebItemModuleDescriptor {
    String getSection();

    WebLink getLink();

    WebIcon getIcon();

    /**
     * Returns the item style as a "class" String consisting of one or more style classes. The default value returned
     * should be an empty String rather than null.
     * <p>
     * Where possible, use of this method is preferred over <code>getIcon</code> as it allows more flexibility for
     * CSS-based web element styling and class-based JavaScript behaviour.
     *
     * @return space-separated list of style classes
     */
    String getStyleClass();

    /**
     * @return The entry point web-resource key.
     * @since 5.1
     */
    String getEntryPoint();
}
