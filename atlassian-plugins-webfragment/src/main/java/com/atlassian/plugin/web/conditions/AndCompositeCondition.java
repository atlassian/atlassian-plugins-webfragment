package com.atlassian.plugin.web.conditions;

import java.util.Map;

import com.atlassian.plugin.web.Condition;

public class AndCompositeCondition extends AbstractCompositeCondition {
    public AndCompositeCondition() {}

    public AndCompositeCondition(Condition... conditions) {
        super(conditions);
    }

    public boolean shouldDisplay(Map<String, Object> context) {
        for (Condition condition : conditions) {
            if (!condition.shouldDisplay(context)) return false;
        }

        return true;
    }
}
