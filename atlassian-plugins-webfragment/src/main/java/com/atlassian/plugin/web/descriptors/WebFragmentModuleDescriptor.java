package com.atlassian.plugin.web.descriptors;

import com.atlassian.plugin.web.model.WebLabel;

/**
 * A convenience interface for web fragment descriptors
 *
 * @deprecated since 6.1.0 use {@link com.atlassian.plugin.web.api.descriptors.WebFragmentModuleDescriptor}
 */
public interface WebFragmentModuleDescriptor<T>
        extends com.atlassian.plugin.web.api.descriptors.WebFragmentModuleDescriptor<T> {
    WebLabel getWebLabel();

    WebLabel getTooltip();
}
