package com.atlassian.plugin.web.model;

import java.util.Collections;
import java.util.Map;

import com.atlassian.plugin.web.ContextProvider;
import com.atlassian.plugin.web.WebFragmentHelper;
import com.atlassian.plugin.web.api.descriptors.WebFragmentModuleDescriptor;

/**
 * Represents web items that can be rendered using velocity, and inject its own context using the {@link ContextProvider}
 */
public abstract class AbstractWebItem {
    private final WebFragmentHelper webFragmentHelper;
    private final ContextProvider contextProvider;
    private final WebFragmentModuleDescriptor descriptor;

    protected AbstractWebItem(
            WebFragmentHelper webFragmentHelper,
            ContextProvider contextProvider,
            WebFragmentModuleDescriptor descriptor) {
        this.webFragmentHelper = webFragmentHelper;
        this.contextProvider = contextProvider;
        this.descriptor = descriptor;
    }

    public Map<String, Object> getContextMap(Map<String, Object> context) {
        if (contextProvider != null) {
            return contextProvider.getContextMap(context);
        }
        return Collections.EMPTY_MAP;
    }

    public WebFragmentHelper getWebFragmentHelper() {
        return webFragmentHelper;
    }

    public WebFragmentModuleDescriptor getDescriptor() {
        return descriptor;
    }
}
