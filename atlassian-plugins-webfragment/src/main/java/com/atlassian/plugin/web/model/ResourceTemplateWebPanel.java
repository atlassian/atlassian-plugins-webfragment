package com.atlassian.plugin.web.model;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Map;

import org.apache.commons.lang3.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.base.Preconditions;

import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.web.api.renderer.RendererException;

/**
 * This class is used for web panel declaration that do not have a custom
 * <code>class</code> attribute in their descriptor, but do have a
 * <code>location</code> attribute in their resource child element, which
 * points to a template file on the (plugin's) classpath.
 *
 * @see com.atlassian.plugin.web.descriptors.DefaultWebPanelModuleDescriptor
 * @since 2.5.0
 */
public class ResourceTemplateWebPanel extends AbstractWebPanel {
    private static final Logger logger = LoggerFactory.getLogger(ResourceTemplateWebPanel.class.getName());
    private String resourceFilename;

    public ResourceTemplateWebPanel(PluginAccessor pluginAccessor) {
        super(pluginAccessor);
    }

    /**
     * Specifies the name of the template file that is to be rendered.
     * This file will be loaded from the (plugin's) classpath.
     *
     * @param resourceFilename the name of the template file that is to be rendered.
     *                         May not be null.
     */
    public void setResourceFilename(String resourceFilename) {
        this.resourceFilename = Preconditions.checkNotNull(resourceFilename, "resourceFilename");
    }

    public void writeHtml(Writer writer, Map<String, Object> context) throws IOException {
        try {
            getRenderer().render(resourceFilename, plugin, context, writer);
        } catch (RendererException e) {
            final String message = String.format("Error rendering WebPanel (%s): %s", resourceFilename, e.getMessage());
            logger.warn(message, e);
            writer.write(StringEscapeUtils.escapeHtml4(message));
        }
    }

    public String getHtml(final Map<String, Object> context) {
        try {
            final StringWriter sink = new StringWriter();
            writeHtml(sink, context);
            return sink.toString();
        } catch (IOException e) {
            // Something went very wrong: we couldn't write to a StringWriter!
            throw new RuntimeException(e);
        }
    }
}
