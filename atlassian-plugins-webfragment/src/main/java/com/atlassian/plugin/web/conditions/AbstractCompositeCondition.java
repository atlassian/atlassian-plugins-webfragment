package com.atlassian.plugin.web.conditions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.Condition;
import com.atlassian.plugin.web.api.baseconditions.CompositeCondition;

public abstract class AbstractCompositeCondition implements Condition, CompositeCondition<Condition> {
    protected List<Condition> conditions = new ArrayList<>();

    protected AbstractCompositeCondition(Condition... conditions) {
        if (conditions != null) {
            this.conditions.addAll(Arrays.asList(conditions));
        }
    }

    public void addCondition(Condition condition) {
        this.conditions.add(condition);
    }

    public void init(Map<String, String> params) throws PluginParseException {}

    public abstract boolean shouldDisplay(Map<String, Object> context);
}
