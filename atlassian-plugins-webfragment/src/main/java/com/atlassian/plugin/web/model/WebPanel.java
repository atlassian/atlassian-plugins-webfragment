package com.atlassian.plugin.web.model;

/**
 * The module that is responsive for providing the raw content for a Web Panel.
 * Whatever is returned by {@link #getHtml(java.util.Map)} is inserted into the
 * host application's page, so it has to be valid HTML.
 * <p>
 * See {@code com.atlassian.plugin.web.descriptors.DefaultWebPanelModuleDescriptor#getModule()}
 *
 * @since 2.5.0
 * @deprecated since 6.1.0 use {@link com.atlassian.plugin.web.api.model.WebPanel}. Will be removed in the next major.
 */
public interface WebPanel extends com.atlassian.plugin.web.api.model.WebPanel {}
