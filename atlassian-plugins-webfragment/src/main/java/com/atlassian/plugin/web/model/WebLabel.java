package com.atlassian.plugin.web.model;

import java.util.Map;
import javax.servlet.http.HttpServletRequest;

/**
 * Represents a plain text, primarily used as a links name
 *
 * @deprecated as of 3.0.2 use {@link com.atlassian.plugin.web.api.WebFragment#getLabel()} if you can.
 */
@Deprecated
public interface WebLabel {
    String getKey();

    String getDisplayableLabel(HttpServletRequest req, Map<String, Object> context);
}
