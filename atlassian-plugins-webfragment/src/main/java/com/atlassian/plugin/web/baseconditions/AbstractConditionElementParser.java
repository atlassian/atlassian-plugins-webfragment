package com.atlassian.plugin.web.baseconditions;

import java.util.List;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.module.Element;
import com.atlassian.plugin.util.Assertions;
import com.atlassian.plugin.web.api.baseconditions.BaseCondition;
import com.atlassian.plugin.web.api.baseconditions.CompositeCondition;

/**
 * This abstract class contains the logic for constructing
 * {@link BaseCondition} objects from a module descriptor's
 * XML element. It provides the logic to implement composite conditions (AND and OR) and inverted conditions.
 *
 * @since v3.0
 */
public abstract class AbstractConditionElementParser<T extends BaseCondition> {
    public static class CompositeType {
        public static final int OR = 0;
        public static final int AND = 1;

        public static int parse(final String type) throws PluginParseException {
            if ("or".equalsIgnoreCase(type)) {
                return CompositeType.OR;
            } else if ("and".equalsIgnoreCase(type)) {
                return CompositeType.AND;
            } else {
                throw new PluginParseException("Invalid condition type specified. type = " + type);
            }
        }
    }

    /**
     * Create a condition for when this web fragment should be displayed.
     *
     * @param element Element of web-section, web-item, or web-panel.
     * @param type    logical operator type
     * @throws com.atlassian.plugin.PluginParseException
     */
    @SuppressWarnings("unchecked")
    public T makeConditions(final Plugin plugin, final Element element, final int type) throws PluginParseException {
        Assertions.notNull("plugin == null", plugin);

        // make single conditions (all Anded together)
        final List<Element> singleConditionElements = element.elements("condition");
        T singleConditions = null;
        if ((singleConditionElements != null) && !singleConditionElements.isEmpty()) {
            singleConditions = makeConditions(plugin, singleConditionElements, type);
        }

        // make composite conditions (logical operator can be specified by
        // "type")
        final List<Element> nestedConditionsElements = element.elements("conditions");
        CompositeCondition<T> nestedConditions = null;
        if ((nestedConditionsElements != null) && !nestedConditionsElements.isEmpty()) {
            nestedConditions = getCompositeCondition(type);
            for (final Element nestedElement : nestedConditionsElements) {
                nestedConditions.addCondition(makeConditions(
                        plugin, nestedElement, CompositeType.parse(nestedElement.attributeValue("type"))));
            }
        }

        if ((singleConditions != null) && (nestedConditions != null)) {
            // Join together the single and composite conditions by this type
            final CompositeCondition<T> compositeCondition = getCompositeCondition(type);
            compositeCondition.addCondition(singleConditions);
            compositeCondition.addCondition((T) nestedConditions);
            return (T) compositeCondition;
        } else if (singleConditions != null) {
            return singleConditions;
        } else if (nestedConditions != null) {
            return (T) nestedConditions;
        }

        return null;
    }

    public T makeConditions(final Plugin plugin, final List<Element> elements, final int type)
            throws PluginParseException {
        if (elements.isEmpty()) {
            return null;
        } else if (elements.size() == 1) {
            return makeCondition(plugin, elements.get(0));
        } else {
            final CompositeCondition<T> compositeCondition = getCompositeCondition(type);
            for (final Element element : elements) {
                compositeCondition.addCondition(makeCondition(plugin, element));
            }

            return (T) compositeCondition;
        }
    }

    public T makeCondition(final Plugin plugin, final Element element) throws PluginParseException {
        T condition = makeConditionImplementation(plugin, element);
        if ((element.attributeValue("invert") != null) && "true".equals(element.attributeValue("invert"))) {
            return invert(condition);
        }
        return condition;
    }

    protected abstract T makeConditionImplementation(final Plugin plugin, final Element element)
            throws PluginParseException;

    protected abstract T invert(T condition);

    protected abstract CompositeCondition<T> createAndCompositeCondition();

    protected abstract CompositeCondition<T> createOrCompositeCondition();

    private CompositeCondition<T> getCompositeCondition(final int type) throws PluginParseException {
        switch (type) {
            case CompositeType.OR:
                return createOrCompositeCondition();
            case CompositeType.AND:
                return createAndCompositeCondition();
            default:
                throw new PluginParseException("Invalid condition type specified. type = " + type);
        }
    }
}
