package com.atlassian.plugin.web.model;

import java.util.Map;
import javax.annotation.Nonnull;

import com.atlassian.plugin.web.api.WebSection;

public class WebSectionImpl extends AbstractWebFragment implements WebSection {
    private final String location;

    WebSectionImpl(
            final String completeKey,
            final String label,
            final String title,
            final String styleClass,
            final String id,
            final Map<String, String> params,
            final int weight,
            final String location) {
        super(completeKey, label, title, styleClass, id, params, weight);
        this.location = location;
    }

    @Nonnull
    @Override
    public String getLocation() {
        return location;
    }

    @Override
    protected String toStringOfFields() {
        return super.toStringOfFields() + ", location=" + location;
    }
}
