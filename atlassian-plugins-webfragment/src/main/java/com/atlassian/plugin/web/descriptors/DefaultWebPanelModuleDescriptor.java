package com.atlassian.plugin.web.descriptors;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.List;
import java.util.Map;

import com.google.common.base.Supplier;
import com.google.common.collect.Maps;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.hostcontainer.HostContainer;
import com.atlassian.plugin.module.Element;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.util.validation.ValidationException;
import com.atlassian.plugin.web.WebInterfaceManager;
import com.atlassian.plugin.web.model.WebPanel;

/**
 * <p>
 * The web panel module declares a single web panel in atlassian-plugin.xml. Its
 * XML element contains a location string that should match existing locations
 * in the host application where web panels can be embedded.
 * </p>
 * <p>
 * A web panel also contains a single resource child element that contains the
 * contents of the web panel. This can be plain HTML, or a (velocity) template
 * to provide dynamic content.
 * </p>
 * <p>
 * A resource element's <code>type</code> attribute identifies the format of the
 * panel's content (currently "static" and "velocity" are supported) which
 * allows the plugin framework to use the appropriate
 * {@link com.atlassian.plugin.web.api.renderer.WebPanelRenderer}.
 * </p>
 * <p>
 * A web panel's resource element can either contain its contents embedded in
 * the resource element itself, as part of the <code>atlassian-plugin.xml</code>
 * file, or it can link to a file on the classpath when the
 * <code>location</code> attribute is used.
 * </p>
 * <b>Examples</b>
 * <p>
 * A web panel that contains static, embedded HTML:
 *
 * <pre>
 *     &lt;web-panel key="myPanel" location="general"&gt;
 *         &lt;resource name="view" type="static"&gt;&lt;![CDATA[&lt;b&gt;Hello World!&lt;/b&gt;]]&gt;&lt;/resource&gt;
 *     &lt;/web-panel&gt;
 * </pre>
 * <p>
 * A web panel that contains an embedded velocity template:
 *
 * <pre>
 *     &lt;web-panel key="myPanel" location="general"&gt;
 *         &lt;resource name="view" type="velocity"&gt;&lt;![CDATA[#set($name = 'foo')My name is $name]]&gt;&lt;/resource&gt;
 *     &lt;/web-panel&gt;
 * </pre>
 *
 * <p>
 * A web panel that contains uses a velocity template that is on the classpath
 * (part of the plugin's jar file):
 *
 * <pre>
 *     &lt;web-panel key="myPanel" location="general"&gt;
 *         &lt;resource name="view" type="velocity" location="templates/pie.vm"/&gt;
 *     &lt;/web-panel&gt;
 * </pre>
 *
 * <p>
 * Finally it is also possible to provide your own custom class that is
 * responsible for producing the panel's HTML, by using the descriptor's
 * <code>class</code> attribute:
 *
 * <pre>
 *     &lt;web-panel key="myPanel" location="general" class="com.example.FooWebPanel"/&gt;
 * </pre>
 * <p>
 * Note that <code>FooWebPanel</code> must implement
 * {@link com.atlassian.plugin.web.model.WebPanel}.
 *
 * @since 2.5.0
 */
public class DefaultWebPanelModuleDescriptor extends AbstractWebFragmentModuleDescriptor<WebPanel>
        implements WebPanelModuleDescriptor {
    /**
     * Host applications should use this string when registering the web panel
     * module descriptor.
     */
    public static final String XML_ELEMENT_NAME = "web-panel";

    private final WebPanelSupplierFactory webPanelSupplierFactory;

    /**
     * These suppliers are used to delay instantiation because the required
     * spring beans are not available for injection during the init() phase.
     */
    private Supplier<com.atlassian.plugin.web.api.model.WebPanel> webPanelFactory;

    private int weight;
    private String location;

    public DefaultWebPanelModuleDescriptor(
            final HostContainer hostContainer,
            final ModuleFactory moduleClassFactory,
            final WebInterfaceManager webInterfaceManager) {
        super(moduleClassFactory, webInterfaceManager);
        this.webPanelSupplierFactory = new WebPanelSupplierFactory(this, hostContainer, moduleFactory);
        this.webInterfaceManager = webInterfaceManager;
    }

    @Override
    public void init(final Plugin plugin, final Element element) throws PluginParseException {
        super.init(plugin, element);

        weight = WeightElementParser.getWeight(element);
        location = element.attributeValue("location");

        webPanelFactory = webPanelSupplierFactory.build(moduleClassName);
    }

    private class ContextAwareWebPanel implements WebPanel {
        private final com.atlassian.plugin.web.api.model.WebPanel delegate;

        private ContextAwareWebPanel(com.atlassian.plugin.web.api.model.WebPanel delegate) {
            this.delegate = delegate;
        }

        public String getHtml(final Map<String, Object> context) {
            var writer = new StringWriter();
            try {
                writeHtml(writer, context);
                return writer.toString();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        public void writeHtml(Writer writer, Map<String, Object> context) throws IOException {
            // PLUGFRAG-47 This should only be called once, Jira has built some assumptions on that....
            var mixedContext = getContextProvider().getContextMap(Maps.newHashMap(context));

            boolean shouldWriteComments = Boolean.getBoolean("atl.html.trace.comments.webpanels.enabled");
            if (shouldWriteComments) {
                writer.write(String.format("<!-- [START] [web-panel] %s -->", getCompleteKey()));
            }
            delegate.writeHtml(writer, mixedContext);
            if (shouldWriteComments) {
                writer.write(String.format("<!-- [END] [web-panel] %s -->", getCompleteKey()));
            }
        }
    }

    @Override
    protected void validate(Element element) {
        super.validate(element);
        if (element.attributeValue("location") == null) {
            throw new ValidationException(
                    "There were validation errors:", List.of("The Web Panel location attribute is required."));
        }
    }

    public String getLocation() {
        return location;
    }

    public int getWeight() {
        return weight;
    }

    @Override
    public WebPanel getModule() {
        return new ContextAwareWebPanel(webPanelFactory.get());
    }

    @Override
    public void enabled() {
        super.enabled();
        webInterfaceManager.refresh();
    }

    @Override
    public void disabled() {
        webInterfaceManager.refresh();
        super.disabled();
    }
}
