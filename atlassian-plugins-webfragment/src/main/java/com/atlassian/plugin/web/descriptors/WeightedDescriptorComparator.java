package com.atlassian.plugin.web.descriptors;

import java.util.Comparator;

import com.atlassian.plugin.web.api.descriptors.WeightedDescriptor;

/**
 * A simple comparator for any weighted descriptor - lowest weights first.
 */
public class WeightedDescriptorComparator<T extends WeightedDescriptor> implements Comparator<T> {
    public int compare(T w1, T w2) {
        /*  SPFE-498 DO NOT change to w1.getWeight() - w2.getWeight()
         *  This will cause an integer underflow. This was found by breaking Confluence's builds which lead to finding
         *  some web-items like 'create page' that rely on being near or exactly Integer#MIN_VALUE
         */
        if (w1.getWeight() < w2.getWeight()) {
            return -1;
        } else if (w1.getWeight() > w2.getWeight()) {
            return 1;
        } else {
            return 0;
        }
    }
}
