package com.atlassian.plugin.web.descriptors;

import java.util.List;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.StateAware;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.descriptors.ModuleDescriptors;
import com.atlassian.plugin.internal.module.Dom4jDelegatingElement;
import com.atlassian.plugin.module.Element;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.web.Condition;
import com.atlassian.plugin.web.ContextProvider;
import com.atlassian.plugin.web.WebInterfaceManager;
import com.atlassian.plugin.web.api.conditions.ConditionLoadingException;
import com.atlassian.plugin.web.api.descriptors.WebFragmentModuleDescriptor;
import com.atlassian.plugin.web.model.DefaultWebLabel;
import com.atlassian.plugin.web.model.WebLabel;

/**
 * An abstract convenience class for web fragment descriptors.
 */
public abstract class AbstractWebFragmentModuleDescriptor<T> extends AbstractModuleDescriptor<T>
        implements StateAware, WebFragmentModuleDescriptor<T> {
    protected WebInterfaceManager webInterfaceManager;
    protected int weight;

    protected Condition condition;
    protected ContextProvider contextProvider;
    protected DefaultWebLabel label;
    protected DefaultWebLabel tooltip;
    private ConditionElementParser conditionElementParser;
    private ContextProviderElementParser contextProviderElementParser;
    private Element descriptorElement;

    protected AbstractWebFragmentModuleDescriptor(final WebInterfaceManager webInterfaceManager) {
        super(ModuleFactory.LEGACY_MODULE_FACTORY);
        setWebInterfaceManager(webInterfaceManager);
    }

    public AbstractWebFragmentModuleDescriptor() {
        super(ModuleFactory.LEGACY_MODULE_FACTORY);
    }

    public AbstractWebFragmentModuleDescriptor(
            ModuleFactory moduleClassFactory, WebInterfaceManager webInterfaceManager) {
        super(moduleClassFactory);
        setWebInterfaceManager(webInterfaceManager);
    }

    @Override
    public void init(final Plugin plugin, final Element element) throws PluginParseException {
        super.init(plugin, element);

        this.descriptorElement = element;
        weight = WeightElementParser.getWeight(element);
    }

    /**
     * Build a condition for when this web fragment should be displayed.
     *
     * @param element Element of web-section or web-item
     * @param type logical operator type
     * @throws PluginParseException when an error occurred
     */
    protected Condition buildConditions(final Element element, final int type) throws PluginParseException {
        return getRequiredConditionElementParser().makeConditions(plugin, element, type);
    }

    protected Condition buildConditions(final List<Element> elements, final int type) throws PluginParseException {
        return getRequiredConditionElementParser().makeConditions(plugin, elements, type);
    }

    protected Condition buildCondition(final Element element) throws PluginParseException {
        return getRequiredConditionElementParser().makeCondition(plugin, element);
    }

    protected ContextProvider buildContextProvider(final Element element) throws PluginParseException {
        return contextProviderElementParser.makeContextProvider(
                plugin,
                new Dom4jDelegatingElement(
                        ((Dom4jDelegatingElement) element).getDelegate().getParent()));
    }

    private ConditionElementParser getRequiredConditionElementParser() {
        if (conditionElementParser == null) {
            throw new IllegalStateException(
                    "ModuleDescriptorHelper not available because the WebInterfaceManager has not been injected.");
        } else {
            return conditionElementParser;
        }
    }

    @Override
    public void enabled() {
        super.enabled();
        // this was moved to the enabled() method because spring beans declared
        // by the plugin are not available for injection during the init() phase
        try {
            contextProvider = contextProviderElementParser.makeContextProvider(plugin, descriptorElement);

            Element labelElement = descriptorElement.element("label");
            if (labelElement != null) {
                label = new DefaultWebLabel(
                        labelElement, webInterfaceManager.getWebFragmentHelper(), contextProvider, this);
            }

            Element tooltipElement = descriptorElement.element("tooltip");
            if (tooltipElement != null) {
                tooltip = new DefaultWebLabel(
                        tooltipElement, webInterfaceManager.getWebFragmentHelper(), contextProvider, this);
            }

            condition = buildConditions(descriptorElement, ConditionElementParser.CompositeType.AND);
        } catch (final PluginParseException e) {
            // is there a better exception to throw?
            throw new RuntimeException("Unable to enable web fragment", e);
        }

        webInterfaceManager.refresh();
    }

    @Override
    public void disabled() {
        condition = null;
        webInterfaceManager.refresh();
        super.disabled();
    }

    public int getWeight() {
        return weight;
    }

    public WebLabel getWebLabel() {
        return label;
    }

    public WebLabel getTooltip() {
        return tooltip;
    }

    public void setWebInterfaceManager(final WebInterfaceManager webInterfaceManager) {
        this.webInterfaceManager = webInterfaceManager;
        this.conditionElementParser = new ConditionElementParser(new ConditionElementParser.ConditionFactory() {
            public Condition create(String className, Plugin plugin) throws ConditionLoadingException {
                return webInterfaceManager.getWebFragmentHelper().loadCondition(className, plugin);
            }
        });
        this.contextProviderElementParser =
                new ContextProviderElementParser(webInterfaceManager.getWebFragmentHelper());
    }

    public Condition getCondition() {
        return condition;
    }

    public ContextProvider getContextProvider() {
        return contextProvider;
    }

    @Override
    public boolean equals(Object obj) {
        return new ModuleDescriptors.EqualsBuilder().descriptor(this).isEqualTo(obj);
    }

    @Override
    public int hashCode() {
        return new ModuleDescriptors.HashCodeBuilder().descriptor(this).toHashCode();
    }
}
