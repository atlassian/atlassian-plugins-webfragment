package com.atlassian.plugin.web.descriptors;

/**
 * A web-section plugin adds extra sections to a particular location.
 *
 * @deprecated since 6.1.0 use {@link com.atlassian.plugin.web.api.descriptors.WebSectionModuleDescriptor}. Will be
 * removed in the next major version.
 */
public interface WebSectionModuleDescriptor
        extends WebFragmentModuleDescriptor<Void>,
                com.atlassian.plugin.web.api.descriptors.WebSectionModuleDescriptor {}
