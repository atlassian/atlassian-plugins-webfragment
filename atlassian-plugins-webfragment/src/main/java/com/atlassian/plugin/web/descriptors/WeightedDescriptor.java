package com.atlassian.plugin.web.descriptors;

/**
 * A simple interface implemented by any weighted descriptors.
 *
 * @deprecated since 6.1.0 use {@link com.atlassian.plugin.web.api.descriptors.WeightedDescriptor}. Will be removed in
 * the next major version.
 */
public interface WeightedDescriptor extends com.atlassian.plugin.web.api.descriptors.WeightedDescriptor {}
