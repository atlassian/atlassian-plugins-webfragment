package com.atlassian.plugin.web.descriptors;

import org.apache.commons.lang3.StringUtils;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.module.Element;
import com.atlassian.plugin.web.WebInterfaceManager;
import com.atlassian.plugin.web.model.DefaultWebIcon;
import com.atlassian.plugin.web.model.DefaultWebLink;
import com.atlassian.plugin.web.model.WebIcon;
import com.atlassian.plugin.web.model.WebLink;

/**
 * Represents a pluggable link.
 */
public class DefaultWebItemModuleDescriptor extends AbstractWebFragmentModuleDescriptor<Void>
        implements WebItemModuleDescriptor {
    private String section;
    private WebIcon icon;
    private DefaultWebLink link;
    private String styleClass;
    private String entryPoint;
    private Element element;

    public DefaultWebItemModuleDescriptor(final WebInterfaceManager webInterfaceManager) {
        super(webInterfaceManager);
    }

    public DefaultWebItemModuleDescriptor() {}

    @Override
    public void init(final Plugin plugin, final Element element) throws PluginParseException {
        super.init(plugin, element);

        this.element = element;
        section = element.attributeValue("section");

        if (element.element("styleClass") != null) {
            styleClass = element.element("styleClass").getTextTrim();
        } else {
            styleClass = "";
        }

        if (element.element("entry-point") != null) {
            entryPoint = element.element("entry-point").getTextTrim();
        } else {
            entryPoint = "";
        }
    }

    public String getSection() {
        return section;
    }

    public WebLink getLink() {
        return link;
    }

    public WebIcon getIcon() {
        return icon;
    }

    public String getStyleClass() {
        return styleClass;
    }

    public String getEntryPoint() {
        if (StringUtils.isNotBlank(entryPoint)) {
            if (entryPoint.matches("^.+:.+$")) {
                return entryPoint;
            }

            return plugin.getKey() + ":" + entryPoint;
        }
        return null;
    }

    @Override
    public void enabled() {
        super.enabled();

        // contextProvider is not available until the module is enabled because they may need to have dependencies
        // injected
        Element iconElement = element.element("icon");
        if (iconElement != null) {
            icon = new DefaultWebIcon(iconElement, webInterfaceManager.getWebFragmentHelper(), contextProvider, this);
        }
        Element linkElement = element.element("link");
        if (linkElement != null) {
            link = new DefaultWebLink(linkElement, webInterfaceManager.getWebFragmentHelper(), contextProvider, this);
        }
    }

    @Override
    public Void getModule() {
        return null;
    }
}
