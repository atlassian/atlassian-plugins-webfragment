package com.atlassian.plugin.web.descriptors;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Nonnull;

import com.google.common.base.Supplier;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.Element;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.util.validation.ValidationException;
import com.atlassian.plugin.web.WebInterfaceManager;
import com.atlassian.plugin.web.api.provider.WebItemProvider;

import static com.google.common.base.Suppliers.memoize;

public class WebItemProviderModuleDescriptor extends AbstractModuleDescriptor<WebItemProvider>
        implements com.atlassian.plugin.web.api.descriptors.WebItemProviderModuleDescriptor<WebItemProvider> {

    private final WebInterfaceManager webInterfaceManager;
    private Supplier<WebItemProvider> itemSupplier;
    private String section;

    public WebItemProviderModuleDescriptor(
            final ModuleFactory moduleFactory, final WebInterfaceManager webInterfaceManager) {
        super(moduleFactory);
        this.webInterfaceManager = webInterfaceManager;
    }

    @Override
    protected void validate(Element element) {
        super.validate(element);
        List<String> violations = new ArrayList<>();
        if (element.attributeValue("class") == null) {
            violations.add("The web item provider class is required.");
        }
        if (element.attributeValue("section") == null) {
            violations.add("Must provide a section that items should be added to.");
        }

        if (!violations.isEmpty()) {
            throw new ValidationException("There were validation errors:", violations);
        }
    }

    @Override
    public void init(@Nonnull final Plugin plugin, @Nonnull final Element element) throws PluginParseException {
        super.init(plugin, element);
        itemSupplier = memoize(() -> {
            var webItemProvider = moduleFactory.createModule(moduleClassName, WebItemProviderModuleDescriptor.this);
            webItemProvider.init(this);
            return webItemProvider;
        });

        section = element.attributeValue("section");
    }

    public String getSection() {
        return section;
    }

    @Override
    public WebItemProvider getModule() {
        return itemSupplier.get();
    }

    @Override
    public void enabled() {
        super.enabled();
        webInterfaceManager.refresh();
    }

    @Override
    public void disabled() {
        super.disabled();
        webInterfaceManager.refresh();
    }
}
