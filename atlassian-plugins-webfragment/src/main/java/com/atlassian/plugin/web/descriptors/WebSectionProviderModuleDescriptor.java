package com.atlassian.plugin.web.descriptors;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Nonnull;

import com.google.common.base.Supplier;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.Element;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.util.validation.ValidationException;
import com.atlassian.plugin.web.WebInterfaceManager;
import com.atlassian.plugin.web.api.provider.WebSectionProvider;

import static com.google.common.base.Suppliers.memoize;

public class WebSectionProviderModuleDescriptor extends AbstractModuleDescriptor<WebSectionProvider>
        implements com.atlassian.plugin.web.api.descriptors.WebSectionProviderModuleDescriptor<WebSectionProvider> {
    private final WebInterfaceManager webInterfaceManager;
    private Supplier<WebSectionProvider> sectionSupplier;
    private String location;

    public WebSectionProviderModuleDescriptor(
            final ModuleFactory moduleFactory, final WebInterfaceManager webInterfaceManager) {
        super(moduleFactory);
        this.webInterfaceManager = webInterfaceManager;
    }

    @Override
    protected void validate(Element element) {
        super.validate(element);
        List<String> violations = new ArrayList<>();
        if (element.attributeValue("class") == null) {
            violations.add("The web section provider class is required");
        }
        if (element.attributeValue("location") == null) {
            violations.add("Must provide a location that sections should be added to.");
        }

        if (!violations.isEmpty()) {
            throw new ValidationException("There were validation errors:", violations);
        }
    }

    @Override
    public void init(@Nonnull final Plugin plugin, @Nonnull final Element element) throws PluginParseException {
        super.init(plugin, element);
        sectionSupplier = memoize(() -> {
            var webSectionProvider =
                    moduleFactory.createModule(moduleClassName, WebSectionProviderModuleDescriptor.this);
            webSectionProvider.init(this);
            return webSectionProvider;
        });
        location = element.attributeValue("location");
    }

    public String getLocation() {
        return location;
    }

    @Override
    public WebSectionProvider getModule() {
        return sectionSupplier.get();
    }

    @Override
    public void enabled() {
        super.enabled();
        webInterfaceManager.refresh();
    }

    @Override
    public void disabled() {
        super.disabled();
        webInterfaceManager.refresh();
    }
}
