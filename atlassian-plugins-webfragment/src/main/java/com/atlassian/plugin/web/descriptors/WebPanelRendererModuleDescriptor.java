package com.atlassian.plugin.web.descriptors;

import java.util.List;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.Element;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.util.validation.ValidationException;
import com.atlassian.plugin.web.api.renderer.WebPanelRenderer;

/**
 * The web panel renderer module is used to add web panel renderers to the
 * plugin system.
 * @since 2.5.0
 * @deprecated since 6.1.0 use {@link com.atlassian.plugin.web.api.descriptors.WebPanelRendererModuleDescriptor} Will be
 * removed in the next major version.
 */
public class WebPanelRendererModuleDescriptor extends AbstractModuleDescriptor<WebPanelRenderer>
        implements com.atlassian.plugin.web.api.descriptors.WebPanelRendererModuleDescriptor<WebPanelRenderer> {
    /**
     * Host applications should use this string when registering the
     * {@link WebPanelRendererModuleDescriptor}.
     */
    public static final String XML_ELEMENT_NAME = "web-panel-renderer";

    private WebPanelRenderer rendererModule;

    public WebPanelRendererModuleDescriptor(ModuleFactory moduleClassFactory) {
        super(moduleClassFactory);
    }

    @Override
    public void init(Plugin plugin, Element element) throws PluginParseException {
        super.init(plugin, element);
    }

    @Override
    protected void validate(Element element) {
        super.validate(element);
        if (element.attributeValue("class") == null) {
            throw new ValidationException("There were validation errors:", List.of("The class is required"));
        }
    }

    @Override
    public void enabled() {
        super.enabled();
        if (!(WebPanelRenderer.class.isAssignableFrom(getModuleClass()))) {
            throw new PluginParseException(String.format(
                    "Supplied module class (%s) is not a %s",
                    getModuleClass().getName(), WebPanelRenderer.class.getName()));
        }
    }

    @Override
    public WebPanelRenderer getModule() {
        if (rendererModule == null) {
            rendererModule = moduleFactory.createModule(moduleClassName, this);
        }
        return rendererModule;
    }
}
