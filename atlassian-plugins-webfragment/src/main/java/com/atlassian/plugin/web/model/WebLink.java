package com.atlassian.plugin.web.model;

import java.util.Map;
import javax.servlet.http.HttpServletRequest;

import com.atlassian.plugin.web.api.descriptors.WebFragmentModuleDescriptor;

/**
 * @deprecated as of 3.0.2 use {@link com.atlassian.plugin.web.api.WebItem} if you can.
 */
public interface WebLink {
    String getRenderedUrl(Map<String, Object> context);

    String getDisplayableUrl(HttpServletRequest req, Map<String, Object> context);

    boolean hasAccessKey();

    String getAccessKey(Map<String, Object> context);

    String getId();

    WebFragmentModuleDescriptor getDescriptor();
}
