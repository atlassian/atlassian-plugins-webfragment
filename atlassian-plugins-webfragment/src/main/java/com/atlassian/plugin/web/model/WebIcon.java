package com.atlassian.plugin.web.model;

/**
 * Represents an icon link
 *
 * @deprecated as of 3.0.2 use {@link com.atlassian.plugin.web.api.WebFragment#getStyleClass()} if you can
 */
public interface WebIcon {
    WebLink getUrl();

    int getWidth();

    int getHeight();
}
