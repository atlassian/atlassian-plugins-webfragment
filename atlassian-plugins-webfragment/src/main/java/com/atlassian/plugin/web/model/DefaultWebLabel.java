package com.atlassian.plugin.web.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import javax.servlet.http.HttpServletRequest;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.loaders.LoaderUtils;
import com.atlassian.plugin.module.Element;
import com.atlassian.plugin.web.ContextProvider;
import com.atlassian.plugin.web.WebFragmentHelper;
import com.atlassian.plugin.web.api.descriptors.WebFragmentModuleDescriptor;

/**
 * A simple bean to represent labels in the web interface.
 */
public class DefaultWebLabel extends AbstractWebItem implements WebLabel {
    private final String key;
    private final String noKeyValue;
    protected SortedMap<String, String> params;

    public DefaultWebLabel(
            Element labelEl,
            WebFragmentHelper webFragmentHelper,
            ContextProvider contextProvider,
            WebFragmentModuleDescriptor descriptor)
            throws PluginParseException {
        super(webFragmentHelper, contextProvider, descriptor);
        this.params = new TreeMap<>(LoaderUtils.getParams(labelEl));
        if (labelEl == null) {
            throw new PluginParseException("You must specify a label for the section.");
        } else {
            this.key = labelEl.attributeValue("key");

            if (this.key == null) {
                this.noKeyValue = labelEl.getTextTrim();
            } else {
                this.noKeyValue = null;
            }
        }
    }

    public String getKey() {
        return key;
    }

    public String getDisplayableLabel(HttpServletRequest req, Map<String, Object> origContext) {
        final Map<String, Object> tmpContext = new HashMap<>(origContext);
        tmpContext.putAll(getContextMap(tmpContext));
        if (key != null) {
            if (params == null || params.isEmpty()) {
                return getWebFragmentHelper().getI18nValue(key, null, tmpContext);
            } else {
                final List<String> arguments = new ArrayList<>();

                // we know here because it's a tree map that the params are in alphabetical order
                for (Map.Entry<String, String> entry : params.entrySet()) {
                    if (entry.getKey().startsWith("param"))
                        arguments.add(getWebFragmentHelper().renderVelocityFragment(entry.getValue(), tmpContext));
                }

                return getWebFragmentHelper().getI18nValue(key, arguments, tmpContext);
            }
        } else {
            return getWebFragmentHelper().renderVelocityFragment(noKeyValue, tmpContext);
        }
    }

    public SortedMap<String, String> getParams() {
        return params;
    }
}
