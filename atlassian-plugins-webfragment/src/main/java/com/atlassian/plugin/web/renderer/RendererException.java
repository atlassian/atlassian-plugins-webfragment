package com.atlassian.plugin.web.renderer;

/**
 * @deprecated since 6.1.0 use {@link com.atlassian.plugin.web.api.renderer.RendererException}. Will be removed in the
 * next major.
 */
public class RendererException extends com.atlassian.plugin.web.api.renderer.RendererException {
    public RendererException(Throwable cause) {
        super(cause);
    }

    public RendererException(String message) {
        super(message);
    }

    public RendererException(String message, Throwable cause) {
        super(message, cause);
    }
}
