package com.atlassian.plugin.web.model;

import java.util.Comparator;

import com.atlassian.plugin.web.api.WebFragment;

/**
 * Comparator that can be used to sort weighted {@link com.atlassian.plugin.web.api.WebFragment}s
 *
 * @since v3.0.2
 */
public class WeightedComparator implements Comparator<WebFragment> {
    public static final WeightedComparator WEIGHTED_FRAGMENT_COMPARATOR = new WeightedComparator();

    @Override
    public int compare(final WebFragment w1, final WebFragment w2) {
        /*  SPFE-498 DO NOT change to w1.getWeight() - w2.getWeight()
         *  This will cause an integer underflow. This was found by breaking Confluence's builds which lead to finding
         *  some web-items like 'create page' that rely on being near or exactly Integer#MIN_VALUE
         */
        if (w1.getWeight() < w2.getWeight()) {
            return -1;
        } else if (w1.getWeight() > w2.getWeight()) {
            return 1;
        } else {
            return 0;
        }
    }
}
