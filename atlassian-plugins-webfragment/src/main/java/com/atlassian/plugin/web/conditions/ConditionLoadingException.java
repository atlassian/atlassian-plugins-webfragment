package com.atlassian.plugin.web.conditions;

/**
 * @deprecated since 6.1.0 use {@link com.atlassian.plugin.web.api.conditions.ConditionLoadingException}. Will be
 * removed in the next major version.
 */
public class ConditionLoadingException extends com.atlassian.plugin.web.api.conditions.ConditionLoadingException {
    public ConditionLoadingException() {}

    public ConditionLoadingException(String string) {
        super(string);
    }

    public ConditionLoadingException(String string, Throwable throwable) {
        super(string, throwable);
    }

    public ConditionLoadingException(Throwable throwable) {
        super(throwable);
    }
}
