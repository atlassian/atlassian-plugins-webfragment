package com.atlassian.plugin.web.baseconditions;

/**
 * Marker interface for conditions
 *
 * @since v3.0
 * @deprecated since 6.1.0 use {@link com.atlassian.plugin.web.api.baseconditions.BaseCondition}. Will be removed in the
 * next major version.
 */
public interface BaseCondition extends com.atlassian.plugin.web.api.baseconditions.BaseCondition {}
