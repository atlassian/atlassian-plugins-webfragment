package com.atlassian.plugin.web.descriptors;

/**
 * A simple interface implemented by any descriptors that support display
 * conditions.
 *
 * @since 2.5.0
 * @deprecated since 6.1.0 use {@link com.atlassian.plugin.web.api.descriptors.ConditionalDescriptor}. Will be removed
 * in the next major.
 */
public interface ConditionalDescriptor extends com.atlassian.plugin.web.api.descriptors.ConditionalDescriptor {}
