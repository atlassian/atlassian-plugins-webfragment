package com.atlassian.plugin.web.renderer;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Writer;
import java.util.Map;

import com.google.common.io.CharStreams;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.web.api.renderer.RendererException;
import com.atlassian.plugin.web.api.renderer.WebPanelRenderer;

/**
 * Static {@link WebPanelRenderer}, just returns the supplied text.
 */
public class StaticWebPanelRenderer implements WebPanelRenderer {
    public static final StaticWebPanelRenderer RENDERER = new StaticWebPanelRenderer();
    public static final String RESOURCE_TYPE = "static";

    public String getResourceType() {
        return RESOURCE_TYPE;
    }

    public void render(String templateName, Plugin plugin, Map<String, Object> context, Writer writer)
            throws RendererException, IOException {
        try (InputStreamReader in = new InputStreamReader(loadTemplate(plugin, templateName))) {
            CharStreams.copy(in, writer);
        }
    }

    public String renderFragment(String fragment, Plugin plugin, Map<String, Object> context) throws RendererException {
        return fragment;
    }

    public void renderFragment(Writer writer, String fragment, Plugin plugin, Map<String, Object> context)
            throws RendererException, IOException {
        writer.write(fragment);
    }

    private InputStream loadTemplate(Plugin plugin, String templateName) throws IOException {
        InputStream in = plugin.getClassLoader().getResourceAsStream(templateName);
        if (in == null) {
            // template not found in the plugin, try the host application:
            if ((in = getClass().getResourceAsStream(templateName)) == null) {
                throw new RendererException(String.format("Static web panel template %s not found.", templateName));
            }
        }
        return in;
    }
}
