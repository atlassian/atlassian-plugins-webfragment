package com.atlassian.plugin.web.model;

import java.util.HashMap;
import java.util.Map;

import com.atlassian.plugin.web.api.WebItem;
import com.atlassian.plugin.web.api.WebSection;

import static com.atlassian.plugin.util.Assertions.notNull;

public class WebFragmentBuilder {
    private String completeKey;
    private String label;
    private String title;
    private String styleClass;
    private String id;
    private Map<String, String> params;
    private final int weight;

    public WebFragmentBuilder(final int weight) {
        this.weight = weight;
        this.params = new HashMap<>(0);
    }

    public WebFragmentBuilder(final String completeKey, final int weight) {
        this.completeKey = completeKey;
        this.weight = weight;
        this.params = new HashMap<>(0);
    }

    public WebFragmentBuilder label(final String label) {
        this.label = label;
        return this;
    }

    public WebFragmentBuilder title(final String title) {
        this.title = title;
        return this;
    }

    public WebFragmentBuilder styleClass(final String styleClass) {
        this.styleClass = styleClass;
        return this;
    }

    public WebFragmentBuilder id(final String id) {
        this.id = id;
        return this;
    }

    public WebFragmentBuilder params(final Map<String, String> params) {
        this.params = new HashMap<>(params);
        return this;
    }

    public WebFragmentBuilder addParam(final String key, final String value) {
        params.put(key, value);
        return this;
    }

    public WebItemBuilder webItem(final String section) {
        return new WebItemBuilder(this, section);
    }

    public WebItemBuilder webItem(final String section, final String entryPoint) {
        WebItemBuilder webItemBuilder = new WebItemBuilder(this, section);
        return webItemBuilder.entryPoint(entryPoint);
    }

    public WebSectionBuilder webSection(final String location) {
        return new WebSectionBuilder(this, location);
    }

    public static class WebItemBuilder {
        private final WebFragmentBuilder fragmentBuilder;
        private final String section;
        private String accessKey;
        private String entryPoint;
        private String url;

        public WebItemBuilder(final WebFragmentBuilder fragmentBuilder, final String section) {
            this.fragmentBuilder = fragmentBuilder;
            this.section = section;
        }

        public WebItemBuilder accessKey(final String accessKey) {
            this.accessKey = accessKey;
            return this;
        }

        public WebItemBuilder entryPoint(final String entryPoint) {
            this.entryPoint = entryPoint;
            return this;
        }

        public WebItemBuilder url(final String url) {
            this.url = url;
            return this;
        }

        public WebItem build() {
            return new WebItemImpl(
                    fragmentBuilder.completeKey,
                    fragmentBuilder.label,
                    fragmentBuilder.title,
                    fragmentBuilder.styleClass,
                    fragmentBuilder.id,
                    fragmentBuilder.params,
                    fragmentBuilder.weight,
                    notNull("section", section),
                    url,
                    accessKey,
                    entryPoint);
        }
    }

    public static class WebSectionBuilder {

        private final WebFragmentBuilder fragmentBuilder;
        private final String location;

        public WebSectionBuilder(final WebFragmentBuilder fragmentBuilder, final String location) {
            this.fragmentBuilder = fragmentBuilder;
            this.location = location;
        }

        public WebSection build() {
            return new WebSectionImpl(
                    fragmentBuilder.completeKey,
                    fragmentBuilder.label,
                    fragmentBuilder.title,
                    fragmentBuilder.styleClass,
                    fragmentBuilder.id,
                    fragmentBuilder.params,
                    fragmentBuilder.weight,
                    notNull("location", location));
        }
    }
}
