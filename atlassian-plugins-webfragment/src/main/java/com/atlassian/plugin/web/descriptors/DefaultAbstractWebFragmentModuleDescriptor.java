package com.atlassian.plugin.web.descriptors;

import java.util.List;
import java.util.Map;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.StateAware;
import com.atlassian.plugin.elements.ResourceDescriptor;
import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.module.Element;
import com.atlassian.plugin.web.Condition;
import com.atlassian.plugin.web.ContextProvider;
import com.atlassian.plugin.web.WebInterfaceManager;
import com.atlassian.plugin.web.api.descriptors.WebFragmentModuleDescriptor;

/**
 * Wrapper for {@link WebFragmentModuleDescriptor}, so that it could be extended by application specific wrappers
 * to provide additional methods.
 */
public class DefaultAbstractWebFragmentModuleDescriptor<T> implements StateAware, WebFragmentModuleDescriptor<T> {
    private final WebFragmentModuleDescriptor<T> decoratedDescriptor;

    public DefaultAbstractWebFragmentModuleDescriptor(final WebFragmentModuleDescriptor<T> abstractDescriptor) {
        decoratedDescriptor = abstractDescriptor;
    }

    @Override
    public void enabled() {
        decoratedDescriptor.enabled();
    }

    @Override
    public void disabled() {
        decoratedDescriptor.disabled();
    }

    protected WebFragmentModuleDescriptor getDecoratedDescriptor() {
        return decoratedDescriptor;
    }

    @Override
    public int getWeight() {
        return decoratedDescriptor.getWeight();
    }

    @Override
    public String getKey() {
        return decoratedDescriptor.getKey();
    }

    @Override
    public T getModule() {
        return null;
    }

    @Override
    public String getI18nNameKey() {
        return decoratedDescriptor.getI18nNameKey();
    }

    @Override
    public String getDescriptionKey() {
        return decoratedDescriptor.getDescriptionKey();
    }

    @Override
    public Plugin getPlugin() {
        return decoratedDescriptor.getPlugin();
    }

    @Override
    public boolean isEnabled() {
        return decoratedDescriptor.isEnabled();
    }

    public void setWebInterfaceManager(final WebInterfaceManager webInterfaceManager) {
        // bit of a hack but it works :)
        if (decoratedDescriptor instanceof AbstractWebFragmentModuleDescriptor) {
            final AbstractWebFragmentModuleDescriptor abstractWebFragmentModuleDescriptor =
                    (AbstractWebFragmentModuleDescriptor) decoratedDescriptor;
            abstractWebFragmentModuleDescriptor.setWebInterfaceManager(webInterfaceManager);
        }
    }

    @Override
    public Condition getCondition() {
        return decoratedDescriptor.getCondition();
    }

    @Override
    public ContextProvider getContextProvider() {
        return decoratedDescriptor.getContextProvider();
    }

    // -----------------------------------------------------------------------------------------
    // ModuleDescriptor methods
    @Override
    public String getCompleteKey() {
        return decoratedDescriptor.getCompleteKey();
    }

    @Override
    public String getPluginKey() {
        return decoratedDescriptor.getPluginKey();
    }

    @Override
    public String getName() {
        return decoratedDescriptor.getName();
    }

    @Override
    public String getDescription() {
        return decoratedDescriptor.getDescription();
    }

    @Override
    public Class<T> getModuleClass() {
        return decoratedDescriptor.getModuleClass();
    }

    @Override
    public void init(final Plugin plugin, final Element element) throws PluginParseException {
        decoratedDescriptor.init(plugin, element);
    }

    @Override
    public boolean isEnabledByDefault() {
        return decoratedDescriptor.isEnabledByDefault();
    }

    @Override
    public boolean isSystemModule() {
        return decoratedDescriptor.isSystemModule();
    }

    @Override
    public void destroy() {
        decoratedDescriptor.destroy();
    }

    @Override
    public Float getMinJavaVersion() {
        return decoratedDescriptor.getMinJavaVersion();
    }

    @Override
    public boolean satisfiesMinJavaVersion() {
        return decoratedDescriptor.satisfiesMinJavaVersion();
    }

    @Override
    public Map<String, String> getParams() {
        return decoratedDescriptor.getParams();
    }

    // ------------------------------------------------------------------------------------------------
    // Resourced methods
    @Override
    public List<ResourceDescriptor> getResourceDescriptors() {
        return decoratedDescriptor.getResourceDescriptors();
    }

    @Override
    public ResourceLocation getResourceLocation(final String type, final String name) {
        return decoratedDescriptor.getResourceLocation(type, name);
    }

    @Override
    public ResourceDescriptor getResourceDescriptor(final String type, final String name) {
        return decoratedDescriptor.getResourceDescriptor(type, name);
    }

    @Override
    public String toString() {
        return decoratedDescriptor.toString();
    }
}
