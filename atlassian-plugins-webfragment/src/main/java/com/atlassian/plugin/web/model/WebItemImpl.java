package com.atlassian.plugin.web.model;

import java.util.Map;
import javax.annotation.Nonnull;

import com.atlassian.plugin.web.api.WebItem;

public class WebItemImpl extends AbstractWebFragment implements WebItem {
    private final String section;
    private final String url;
    private final String accessKey;
    private final String entryPoint;

    WebItemImpl(
            final String completeKey,
            final String label,
            final String title,
            final String styleClass,
            final String id,
            final Map<String, String> params,
            final int weight,
            final String section,
            final String url,
            final String accessKey,
            final String entryPoint) {
        super(completeKey, label, title, styleClass, id, params, weight);
        this.section = section;
        this.url = url;
        this.accessKey = accessKey;
        this.entryPoint = entryPoint;
    }

    @Nonnull
    @Override
    public String getSection() {
        return section;
    }

    @Nonnull
    @Override
    public String getUrl() {
        return url;
    }

    @Override
    public String getAccessKey() {
        return accessKey;
    }

    @Override
    protected String toStringOfFields() {
        return super.toStringOfFields() + ", section=" + section + ", url=" + url + ", accessKey=" + accessKey
                + ", entryPoint=" + entryPoint;
    }

    @Nonnull
    public String getEntryPoint() {
        return entryPoint;
    }
}
