package com.atlassian.plugin.web.descriptors;

/**
 * Makes a plugin module aware of its Velocity context. Web modules should
 * implement this interface if they want to modify the Velocity context used to
 * render their content.
 *
 * @deprecated since 6.1.0 use {@link com.atlassian.plugin.web.api.descriptors.ContextAware}. Will be removed in the
 * next major.
 */
public interface ContextAware extends com.atlassian.plugin.web.api.descriptors.ContextAware {}
