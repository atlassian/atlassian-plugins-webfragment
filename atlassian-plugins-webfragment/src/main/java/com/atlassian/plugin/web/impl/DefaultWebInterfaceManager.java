package com.atlassian.plugin.web.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.base.Function;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.Iterables;

import com.atlassian.annotations.VisibleForTesting;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.web.WebFragmentHelper;
import com.atlassian.plugin.web.api.DynamicWebInterfaceManager;
import com.atlassian.plugin.web.api.WebItem;
import com.atlassian.plugin.web.api.WebSection;
import com.atlassian.plugin.web.api.descriptors.WebFragmentModuleDescriptor;
import com.atlassian.plugin.web.descriptors.WebItemModuleDescriptor;
import com.atlassian.plugin.web.descriptors.WebItemProviderModuleDescriptor;
import com.atlassian.plugin.web.descriptors.WebPanelModuleDescriptor;
import com.atlassian.plugin.web.descriptors.WebSectionModuleDescriptor;
import com.atlassian.plugin.web.descriptors.WebSectionProviderModuleDescriptor;
import com.atlassian.plugin.web.descriptors.WeightedDescriptorComparator;
import com.atlassian.plugin.web.model.WebFragmentBuilder;
import com.atlassian.plugin.web.model.WebLink;
import com.atlassian.plugin.web.model.WebPanel;
import com.atlassian.util.profiling.Ticker;

import static com.google.common.collect.Iterables.transform;
import static com.google.common.collect.Lists.newArrayList;
import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toList;

import static com.atlassian.plugin.web.model.WeightedComparator.WEIGHTED_FRAGMENT_COMPARATOR;
import static com.atlassian.util.profiling.Metrics.metric;

/**
 * Stores and manages flexible web interface sections available in the system.
 */
public class DefaultWebInterfaceManager implements DynamicWebInterfaceManager {

    public static final WeightedDescriptorComparator WEIGHTED_DESCRIPTOR_COMPARATOR =
            new WeightedDescriptorComparator();

    @VisibleForTesting
    public static final String CONDITION_METRIC_KEY = "web.fragment.condition";

    @VisibleForTesting
    public static final String FRAGMENT_LOCATION_TAG_KEY = "fragmentLocation";

    @VisibleForTesting
    public static final String CONDITION_CLASSNAME_TAG_KEY = "conditionClassName";

    private static final Logger log = LoggerFactory.getLogger(DefaultWebInterfaceManager.class);
    private static final long CACHE_EXPIRY =
            Long.getLong("com.atlassian.plugin.web.interface.caches.timeout.sec", 60 * 60);

    private final LoadingCache<String, List<WebSectionModuleDescriptor>> sections = CacheBuilder.newBuilder()
            .expireAfterAccess(CACHE_EXPIRY, TimeUnit.SECONDS)
            .build(new WebSectionCacheLoader());
    private final LoadingCache<String, List<WebItemModuleDescriptor>> items = CacheBuilder.newBuilder()
            .expireAfterAccess(CACHE_EXPIRY, TimeUnit.SECONDS)
            .build(new WebItemCacheLoader());
    private final LoadingCache<String, List<WebPanelModuleDescriptor>> panels = CacheBuilder.newBuilder()
            .expireAfterAccess(CACHE_EXPIRY, TimeUnit.SECONDS)
            .build(new WebPanelCacheLoader());
    private final LoadingCache<String, List<WebItemProviderModuleDescriptor>> itemProviders = CacheBuilder.newBuilder()
            .expireAfterAccess(CACHE_EXPIRY, TimeUnit.SECONDS)
            .build(new WebItemProviderCacheLoader());
    private final LoadingCache<String, List<WebSectionProviderModuleDescriptor>> sectionProviders =
            CacheBuilder.newBuilder()
                    .expireAfterAccess(CACHE_EXPIRY, TimeUnit.SECONDS)
                    .build(new WebSectionProviderCacheLoader());

    @Nullable
    private PluginAccessor pluginAccessor;

    @Nullable
    private WebFragmentHelper webFragmentHelper;

    // START OF --- NO ARGS CONSTRUCTOR BLOCK

    /**
     * A no-arg constructor, probably used for lazy-initialization of beans
     *
     * @deprecated since 7.0.0-m02, use {@link DefaultWebInterfaceManager(PluginAccessor, WebFragmentHelper)} instead,
     * this constructor will be removed by 7.0.0. The problem with this is that cache can be invalid since this class is
     * no longer controlling the time of the cache rebuild.
     */
    public DefaultWebInterfaceManager() {
        if (Boolean.parseBoolean("atlassian.dev.mode")) {
            log.error("The no-args DefaultWebInterfaceManager() constructor is deprecated and will be removed by "
                    + "7.0.0 reach out in #server-frontend if that's a problem");
        }
        refresh();
    }

    public void setPluginAccessor(PluginAccessor pluginAccessor) {
        this.pluginAccessor = pluginAccessor;
    }

    public void setWebFragmentHelper(WebFragmentHelper webFragmentHelper) {
        this.webFragmentHelper = webFragmentHelper;
    }
    // END OF --- NO ARGS CONSTRUCTOR BLOCK

    public DefaultWebInterfaceManager(PluginAccessor pluginAccessor, WebFragmentHelper webFragmentHelper) {
        this.pluginAccessor = pluginAccessor;
        this.webFragmentHelper = webFragmentHelper;
        refresh();
    }

    public boolean hasSectionsForLocation(String location) {
        return !Iterables.isEmpty(getWebSections(location, Collections.emptyMap()));
    }

    public List<WebSectionModuleDescriptor> getSections(String location) {
        return location == null ? Collections.emptyList() : sections.getUnchecked(location);
    }

    public List<WebSectionModuleDescriptor> getDisplayableSections(String location, Map<String, Object> context) {
        return filterFragmentsByCondition(location, getSections(location), context);
    }

    public List<WebItemModuleDescriptor> getItems(String section) {
        return section == null ? Collections.emptyList() : items.getUnchecked(section);
    }

    public List<WebItemModuleDescriptor> getDisplayableItems(String section, Map<String, Object> context) {
        return filterFragmentsByCondition(section, getItems(section), context);
    }

    public List<WebPanel> getDisplayableWebPanels(String location, Map<String, Object> context) {
        return toWebPanels(getDisplayableWebPanelDescriptors(location, context));
    }

    public List<WebPanelModuleDescriptor> getDisplayableWebPanelDescriptors(
            String location, Map<String, Object> context) {
        return filterFragmentsByCondition(location, getWebPanelDescriptors(location), context);
    }

    public List<WebPanel> getWebPanels(String location) {
        return toWebPanels(getWebPanelDescriptors(location));
    }

    private List<WebPanel> toWebPanels(List<WebPanelModuleDescriptor> descriptors) {
        return descriptors.stream().map(ModuleDescriptor::getModule).collect(toList());
    }

    public List<WebPanelModuleDescriptor> getWebPanelDescriptors(String location) {
        return location == null ? Collections.emptyList() : panels.getUnchecked(location);
    }

    private <T extends WebFragmentModuleDescriptor<?>> List<T> filterFragmentsByCondition(
            String fragmentLocation, List<T> relevantItems, Map<String, Object> context) {
        if (relevantItems.isEmpty()) {
            return relevantItems;
        }

        List<T> result = new ArrayList<>(relevantItems);
        Iterator<T> iterator = result.iterator();
        while (iterator.hasNext()) {
            T descriptor = iterator.next();

            try {
                if (descriptor.getCondition() != null) {
                    final String conditionClassName =
                            descriptor.getCondition().getClass().getName();
                    try (Ticker ignoredTicker = metric(CONDITION_METRIC_KEY)
                            .fromPluginKey(descriptor.getPluginKey())
                            .tag(CONDITION_CLASSNAME_TAG_KEY, conditionClassName)
                            .tag(FRAGMENT_LOCATION_TAG_KEY, fragmentLocation)
                            .withAnalytics()
                            .startTimer()) {
                        if (!descriptor.getCondition().shouldDisplay(context)) {
                            iterator.remove();
                        }
                    }
                }
            } catch (Throwable t) {
                log.error(
                        "Could not evaluate condition '" + descriptor.getCondition() + "' for descriptor: "
                                + descriptor,
                        t);
                iterator.remove();
            }
        }

        return result;
    }

    public void refresh() {
        sections.invalidateAll();
        items.invalidateAll();
        panels.invalidateAll();
        itemProviders.invalidateAll();
        sectionProviders.invalidateAll();
    }

    public WebFragmentHelper getWebFragmentHelper() {
        return webFragmentHelper;
    }

    @Override
    public Iterable<WebItem> getWebItems(final String section, final Map<String, Object> context) {
        return getDynamicWebItems(getItems(section), section, context);
    }

    @Override
    public Iterable<WebItem> getDisplayableWebItems(final String section, final Map<String, Object> context) {
        return getDynamicWebItems(getDisplayableItems(section, context), section, context);
    }

    @Override
    public Iterable<WebSection> getWebSections(final String location, final Map<String, Object> context) {
        return getDynamicWebSections(getSections(location), location, context);
    }

    @Override
    public Iterable<WebSection> getDisplayableWebSections(final String location, final Map<String, Object> context) {
        return getDynamicWebSections(getDisplayableSections(location, context), location, context);
    }

    private Iterable<WebItem> getDynamicWebItems(
            final List<WebItemModuleDescriptor> staticItems, final String section, final Map<String, Object> context) {
        final List<WebItem> dynamicWebItems = newArrayList(transform(staticItems, new WebItemConverter(context)));
        for (WebItemProviderModuleDescriptor itemProvider : itemProviders.getUnchecked(section)) {
            try {
                final Optional<Iterable<WebItem>> providedItems =
                        ofNullable(itemProvider.getModule().getItems(context));
                providedItems.ifPresent(webItems -> Iterables.addAll(dynamicWebItems, webItems));
            } catch (RuntimeException e) {
                if (log.isDebugEnabled()) {
                    log.error(
                            "WebItemProvider from module '{}' threw an error '{}'. Web-items provided by this provider will be ignored.",
                            itemProvider.getCompleteKey(),
                            e.getMessage(),
                            e);
                } else {
                    log.error(
                            "WebItemProvider from module '{}' threw an error '{}'. Web-items provided by this provider will be ignored.",
                            itemProvider.getCompleteKey(),
                            e.getMessage());
                }
            }
        }

        dynamicWebItems.sort(WEIGHTED_FRAGMENT_COMPARATOR);
        return dynamicWebItems;
    }

    private Iterable<WebSection> getDynamicWebSections(
            final List<WebSectionModuleDescriptor> staticSections,
            final String location,
            final Map<String, Object> context) {
        final List<WebSection> dynamicWebSections =
                newArrayList(transform(staticSections, new WebSectionConverter(context)));
        for (WebSectionProviderModuleDescriptor provider : sectionProviders.getUnchecked(location)) {
            try {
                final Optional<Iterable<WebSection>> sections =
                        ofNullable(provider.getModule().getSections(context));
                sections.ifPresent(webSections -> Iterables.addAll(dynamicWebSections, webSections));
            } catch (RuntimeException e) {
                if (log.isDebugEnabled()) {
                    log.error(
                            "WebSectionProvider from module '{}' threw an error '{}'. Web-sections provided by this provider will be ignored.",
                            provider.getCompleteKey(),
                            e.getMessage(),
                            e);
                } else {
                    log.error(
                            "WebSectionProvider from module '{}' threw an error '{}'. Web-sections provided by this provider will be ignored.",
                            provider.getCompleteKey(),
                            e.getMessage());
                }
            }
        }

        dynamicWebSections.sort(WEIGHTED_FRAGMENT_COMPARATOR);
        return dynamicWebSections;
    }

    private static class WebItemConverter implements Function<WebItemModuleDescriptor, WebItem> {
        private final Map<String, Object> context;

        public WebItemConverter(final Map<String, Object> context) {
            this.context = context;
        }

        @Override
        public WebItem apply(final WebItemModuleDescriptor input) {
            final WebFragmentBuilder builder = new WebFragmentBuilder(input.getCompleteKey(), input.getWeight());
            builder.styleClass(input.getStyleClass());
            if (input.getWebLabel() != null) {
                builder.label(input.getWebLabel().getDisplayableLabel(null, context));
            }
            if (input.getTooltip() != null) {
                builder.title(input.getTooltip().getDisplayableLabel(null, context));
            }
            if (input.getParams() != null) {
                builder.params(input.getParams());
            }

            // icon URLs are pretty rare since we use styleclasses with background icons so
            // this is here for backwards compatibility for those rare cases where a dynamic rendered URL may be
            // necessary.
            if (input.getIcon() != null && input.getIcon().getUrl() != null) {
                builder.addParam("iconUrl", input.getIcon().getUrl().getRenderedUrl(context));
            }

            final WebFragmentBuilder.WebItemBuilder webItemBuilder =
                    builder.webItem(input.getSection(), input.getEntryPoint());
            final WebLink link = input.getLink();
            if (link != null) {
                builder.id(link.getId());
                if (link.hasAccessKey()) {
                    webItemBuilder.accessKey(link.getAccessKey(context));
                }
                final Optional<HttpServletRequest> httpRequest =
                        ofNullable((HttpServletRequest) context.get("request"));
                final String url = httpRequest.isPresent()
                        ? link.getDisplayableUrl(httpRequest.get(), context)
                        : link.getRenderedUrl(context);
                webItemBuilder.url(url);
            }

            return webItemBuilder.build();
        }
    }

    private static class WebSectionConverter implements Function<WebSectionModuleDescriptor, WebSection> {
        private final Map<String, Object> context;

        public WebSectionConverter(final Map<String, Object> context) {
            this.context = context;
        }

        @Override
        public WebSection apply(final WebSectionModuleDescriptor input) {
            final WebFragmentBuilder builder = new WebFragmentBuilder(input.getCompleteKey(), input.getWeight());
            builder.id(input.getKey());
            if (input.getWebLabel() != null) {
                builder.label(input.getWebLabel().getDisplayableLabel(null, context));
            }
            if (input.getTooltip() != null) {
                builder.title(input.getTooltip().getDisplayableLabel(null, context));
            }
            if (input.getParams() != null) {
                builder.params(input.getParams());
            }

            return builder.webSection(input.getLocation()).build();
        }
    }

    private class WebSectionCacheLoader extends CacheLoader<String, List<WebSectionModuleDescriptor>> {
        @Override
        public List<WebSectionModuleDescriptor> load(final String location) throws Exception {
            final List<com.atlassian.plugin.web.api.descriptors.WebSectionModuleDescriptor>
                    webSectionModuleDescriptors = pluginAccessor.getEnabledModuleDescriptorsByClass(
                            com.atlassian.plugin.web.api.descriptors.WebSectionModuleDescriptor.class);
            return webSectionModuleDescriptors.stream()
                    .filter(webSectionModuleDescriptor ->
                            Objects.equals(location, webSectionModuleDescriptor.getLocation()))
                    .sorted(WEIGHTED_DESCRIPTOR_COMPARATOR)
                    .toList();
        }
    }

    private class WebItemCacheLoader extends CacheLoader<String, List<WebItemModuleDescriptor>> {
        @Override
        public List<WebItemModuleDescriptor> load(final String section) {
            final List<com.atlassian.plugin.web.api.descriptors.WebItemModuleDescriptor> webItemModuleDescriptors =
                    pluginAccessor.getEnabledModuleDescriptorsByClass(
                            com.atlassian.plugin.web.api.descriptors.WebItemModuleDescriptor.class);
            return webItemModuleDescriptors.stream()
                    .filter(descriptor -> Objects.equals(section, descriptor.getSection()))
                    .sorted(WEIGHTED_DESCRIPTOR_COMPARATOR)
                    .toList();
        }
    }

    private class WebPanelCacheLoader extends CacheLoader<String, List<WebPanelModuleDescriptor>> {
        @Override
        public List<WebPanelModuleDescriptor> load(final String location) throws Exception {
            final List<com.atlassian.plugin.web.api.descriptors.WebPanelModuleDescriptor> webPanelModuleDescriptors =
                    pluginAccessor.getEnabledModuleDescriptorsByClass(
                            com.atlassian.plugin.web.api.descriptors.WebPanelModuleDescriptor.class);
            return webPanelModuleDescriptors.stream()
                    .filter(webPanelModuleDescriptor ->
                            Objects.equals(location, webPanelModuleDescriptor.getLocation()))
                    .sorted(WEIGHTED_DESCRIPTOR_COMPARATOR)
                    .toList();
        }
    }

    private class WebItemProviderCacheLoader extends CacheLoader<String, List<WebItemProviderModuleDescriptor>> {
        @Override
        public List<WebItemProviderModuleDescriptor> load(final String section) throws Exception {
            final List<WebItemProviderModuleDescriptor> webItemProviderModuleDescriptors =
                    pluginAccessor.getEnabledModuleDescriptorsByClass(WebItemProviderModuleDescriptor.class);
            return webItemProviderModuleDescriptors.stream()
                    .filter(webItemProviderModuleDescriptor ->
                            Objects.equals(webItemProviderModuleDescriptor.getSection(), section))
                    .collect(toList());
        }
    }

    private class WebSectionProviderCacheLoader extends CacheLoader<String, List<WebSectionProviderModuleDescriptor>> {
        @Override
        public List<WebSectionProviderModuleDescriptor> load(final String location) throws Exception {
            final List<WebSectionProviderModuleDescriptor> webSectionProviderModuleDescriptors =
                    pluginAccessor.getEnabledModuleDescriptorsByClass(WebSectionProviderModuleDescriptor.class);
            return webSectionProviderModuleDescriptors.stream()
                    .filter(webSectionProviderModuleDescriptor ->
                            Objects.equals(webSectionProviderModuleDescriptor.getLocation(), location))
                    .collect(toList());
        }
    }
}
