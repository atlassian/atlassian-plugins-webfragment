package com.atlassian.plugin.web.baseconditions;

/**
 * Interface for composite conditions
 *
 * @since v3.0
 * @deprecated since 6.1.0 use {@link com.atlassian.plugin.web.api.baseconditions.CompositeCondition}. Will be removed
 * in the next major version.
 */
public interface CompositeCondition<T extends BaseCondition>
        extends com.atlassian.plugin.web.api.baseconditions.CompositeCondition<T> {}
