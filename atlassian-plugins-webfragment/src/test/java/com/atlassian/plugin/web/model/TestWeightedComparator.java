package com.atlassian.plugin.web.model;

import org.junit.Before;
import org.junit.Test;

import com.atlassian.plugin.web.api.WebFragment;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.number.OrderingComparison.greaterThan;
import static org.hamcrest.number.OrderingComparison.lessThan;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestWeightedComparator {
    private static final int MAX_NEGATIVE = Integer.MIN_VALUE;
    private static final int LITTLE_NEGATIVE = -100;
    private static final int LITTLE_POSITIVE = 100;
    private static final int MAX_POSITIVE = Integer.MAX_VALUE;

    WebFragment w1;
    WebFragment w2;
    WeightedComparator comparator;

    @Before
    public void setup() {
        w1 = mock(WebFragment.class);
        w2 = mock(WebFragment.class);
        comparator = new WeightedComparator();
    }

    @Test
    public void whenEqual_compare_should_returnZero() {
        when(w1.getWeight()).thenReturn(LITTLE_POSITIVE);
        when(w2.getWeight()).thenReturn(LITTLE_POSITIVE);

        final int comparisonResult = comparator.compare(w1, w2);

        assertThat(comparisonResult, is(equalTo(0)));
    }

    @Test
    public void whenTheFormerIsLessThanTheLatter_compare_should_returnANegativeValue() {
        when(w1.getWeight()).thenReturn(LITTLE_NEGATIVE);
        when(w2.getWeight()).thenReturn(LITTLE_POSITIVE);

        final int comparisonResult = comparator.compare(w1, w2);

        assertThat(comparisonResult, is(lessThan(0)));
    }

    @Test
    public void whenTheFormerIsGreaterThanTheLatter_compare_should_returnAPositiveValue() {
        when(w1.getWeight()).thenReturn(LITTLE_POSITIVE);
        when(w2.getWeight()).thenReturn(LITTLE_NEGATIVE);

        final int comparisonResult = comparator.compare(w1, w2);

        assertThat(comparisonResult, is(greaterThan(0)));
    }

    @Test
    public void compare_should_notUnderflow() {
        when(w1.getWeight()).thenReturn(MAX_NEGATIVE);
        when(w2.getWeight()).thenReturn(LITTLE_POSITIVE);

        final int comparisonResult = comparator.compare(w1, w2);

        assertThat(comparisonResult, is(lessThan(0)));
    }

    @Test
    public void compare_should_notOverflow() {
        when(w1.getWeight()).thenReturn(MAX_POSITIVE);
        when(w2.getWeight()).thenReturn(LITTLE_NEGATIVE);

        final int comparisonResult = comparator.compare(w1, w2);

        assertThat(comparisonResult, is(greaterThan(0)));
    }
}
