package com.atlassian.plugin.web.model;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Collections;
import java.util.Map;

import junit.framework.TestCase;

import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.web.api.descriptors.WebPanelRendererModuleDescriptor;
import com.atlassian.plugin.web.api.renderer.RendererException;
import com.atlassian.plugin.web.api.renderer.WebPanelRenderer;
import com.atlassian.plugin.web.renderer.StaticWebPanelRenderer;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AbstractWebPanelTest extends TestCase {
    public void testStaticGetHtml() throws IOException {
        AbstractWebPanel panel = new MyAbstractWebPanel();
        panel.setResourceType(StaticWebPanelRenderer.RESOURCE_TYPE);

        assertEquals("cheese", panel.getHtml(Collections.emptyMap()));
    }

    public void testStaticWriteHtml() throws IOException {
        AbstractWebPanel panel = new MyAbstractWebPanel();
        panel.setResourceType(StaticWebPanelRenderer.RESOURCE_TYPE);

        StringWriter out = new StringWriter();
        panel.writeHtml(out, Collections.emptyMap());
        assertEquals("cheese", out.toString());
    }

    public void testUnsupportedRendererType() {
        final PluginAccessor accessorMock = mock(PluginAccessor.class);
        when(accessorMock.getEnabledModuleDescriptorsByClass(WebPanelRendererModuleDescriptor.class))
                .thenReturn(Collections.emptyList());

        AbstractWebPanel panel = new AbstractWebPanel(accessorMock) {
            public String getHtml(Map<String, Object> context) {
                try {
                    getRenderer();
                    fail();
                } catch (RendererException re) {
                    // expected
                }
                return null;
            }
        };

        panel.setResourceType("unsupported-type");
        panel.getHtml(null);
    }

    public void testSupportedRendererType() {
        final PluginAccessor accessorMock = mock(PluginAccessor.class);
        final WebPanelRenderer velocityRenderer = mock(WebPanelRenderer.class);
        when(velocityRenderer.getResourceType()).thenReturn("velocity");
        final WebPanelRenderer unsupportedRenderer = mock(WebPanelRenderer.class);
        when(unsupportedRenderer.getResourceType()).thenReturn("unsupported-type");
        WebPanelTestUtils.mockPluginAccessorReturning(accessorMock, unsupportedRenderer, velocityRenderer);
        AbstractWebPanel panel = new AbstractWebPanel(accessorMock) {
            public String getHtml(Map<String, Object> context) {
                final WebPanelRenderer webPanelRenderer = getRenderer();
                assertNotNull(webPanelRenderer);
                assertEquals(velocityRenderer, webPanelRenderer);
                return null;
            }
        };

        panel.setResourceType("velocity");
        panel.getHtml(null);
    }

    private static class MyAbstractWebPanel extends AbstractWebPanel {
        public MyAbstractWebPanel() {
            super(null);
        }

        public String getHtml(Map<String, Object> context) {
            final WebPanelRenderer renderer = getRenderer();
            assertEquals(renderer, StaticWebPanelRenderer.RENDERER);
            return "cheese";
        }
    }
}
