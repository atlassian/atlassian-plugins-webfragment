package com.atlassian.plugin.web;

import java.util.Objects;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import com.atlassian.plugin.web.api.WebItem;

/**
 * Deep matcher for {@link com.atlassian.plugin.web.api.WebItem}.
 */
public class EqWebItem extends TypeSafeMatcher<WebItem> {
    private final WebItem expected;

    public EqWebItem(final WebItem expected) {
        this.expected = expected;
    }

    @Override
    public boolean matchesSafely(WebItem item) {
        return Objects.equals(expected.getId(), item.getId())
                && Objects.equals(expected.getAccessKey(), item.getAccessKey())
                && Objects.equals(expected.getSection(), item.getSection())
                && Objects.equals(expected.getUrl(), item.getUrl())
                && Objects.equals(expected.getLabel(), item.getLabel())
                && Objects.equals(expected.getStyleClass(), item.getStyleClass())
                && Objects.equals(expected.getTitle(), item.getTitle())
                && expected.getWeight() == item.getWeight()
                && expected.getParams().equals(item.getParams());
    }

    public void describeTo(Description description) {
        description.appendText("did not match ").appendValue(expected);
    }

    @Factory
    public static Matcher<WebItem> eqWebItem(final WebItem expected) {
        return new EqWebItem(expected);
    }
}
