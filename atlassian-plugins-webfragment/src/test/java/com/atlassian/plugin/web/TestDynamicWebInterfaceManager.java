package com.atlassian.plugin.web;

import java.util.Collections;
import java.util.Map;

import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import com.google.common.collect.Lists;

import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.internal.module.Dom4jDelegatingElement;
import com.atlassian.plugin.web.api.DynamicWebInterfaceManager;
import com.atlassian.plugin.web.api.WebItem;
import com.atlassian.plugin.web.api.WebSection;
import com.atlassian.plugin.web.api.provider.WebItemProvider;
import com.atlassian.plugin.web.api.provider.WebSectionProvider;
import com.atlassian.plugin.web.conditions.NeverDisplayCondition;
import com.atlassian.plugin.web.descriptors.MockWebFragmentHelper;
import com.atlassian.plugin.web.descriptors.WebFragmentModuleDescriptor;
import com.atlassian.plugin.web.descriptors.WebItemModuleDescriptor;
import com.atlassian.plugin.web.descriptors.WebItemProviderModuleDescriptor;
import com.atlassian.plugin.web.descriptors.WebSectionModuleDescriptor;
import com.atlassian.plugin.web.descriptors.WebSectionProviderModuleDescriptor;
import com.atlassian.plugin.web.impl.DefaultWebInterfaceManager;
import com.atlassian.plugin.web.model.DefaultWebLink;
import com.atlassian.plugin.web.model.WebFragmentBuilder;
import com.atlassian.plugin.web.model.WebLabel;

import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.when;

import static com.atlassian.plugin.web.EqWebItem.eqWebItem;
import static com.atlassian.plugin.web.EqWebSection.eqWebSection;

@RunWith(MockitoJUnitRunner.Silent.class)
public class TestDynamicWebInterfaceManager {
    private static final String SAMPLE_SECTION = "top.nav.sample";
    private static final String OTHER_SECTION = "top.nav.other";

    private static final WebItem STATIC_WEB_ITEM_1 = new WebFragmentBuilder(0)
            .styleClass("test-link")
            .title("test link tooltip")
            .label("Test link 1")
            .webItem(SAMPLE_SECTION)
            .url("/rest/api/1")
            .accessKey("a")
            .build();
    private static final WebItem STATIC_WEB_ITEM_2 = new WebFragmentBuilder(10)
            .label("Test link 2")
            .webItem(SAMPLE_SECTION)
            .url("/rest/api/2")
            .build();
    private static final WebItem DYNAMIC_WEB_ITEM_1 = new WebFragmentBuilder(5)
            .styleClass("dynamic-link")
            .title("test dynamic tooltip")
            .label("Dynamic Test 1")
            .webItem(SAMPLE_SECTION)
            .url("/rest/issue/key/1")
            .accessKey("a")
            .build();
    private static final WebItem DYNAMIC_WEB_ITEM_2 = new WebFragmentBuilder(15)
            .webItem(SAMPLE_SECTION)
            .url("/rest/issue/key/1")
            .build();

    private static final WebSection STATIC_WEB_SECTION_1 = new WebFragmentBuilder(0)
            .title("test section tooltip")
            .label("Test section 1")
            .webSection(SAMPLE_SECTION)
            .build();
    private static final WebSection STATIC_WEB_SECTION_2 = new WebFragmentBuilder(10)
            .label("Test section 2")
            .webSection(SAMPLE_SECTION)
            .build();
    private static final WebSection DYNAMIC_WEB_SECTION_1 = new WebFragmentBuilder(5)
            .styleClass("dynamic-section")
            .title("test dynamic section tooltip")
            .label("Dynamic Section 1")
            .webSection(SAMPLE_SECTION)
            .build();
    private static final WebSection DYNAMIC_WEB_SECTION_2 =
            new WebFragmentBuilder(15).webSection(SAMPLE_SECTION).build();

    @Mock
    private PluginAccessor pluginAccessor;

    @Mock
    private WebFragmentHelper webFragmentHelper;

    private DynamicWebInterfaceManager defaultWebInterfaceManager;

    @Before
    public void setUp() {
        defaultWebInterfaceManager = new DefaultWebInterfaceManager(pluginAccessor, webFragmentHelper);

        // add some static web-items
        final WebItemModuleDescriptor webItemDescriptor1 = initWebItem(
                SAMPLE_SECTION, 0, "/rest/api/1", "test-link", "a", "test link tooltip", "Test link 1", false);
        final WebItemModuleDescriptor webItemDescriptor2 =
                initWebItem(SAMPLE_SECTION, 10, "/rest/api/2", null, null, null, "Test link 2", true);
        final WebItemModuleDescriptor webItemDescriptor3 =
                initWebItem(OTHER_SECTION, 10, "/rest/api/2", null, null, null, "Test link 2", false);
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(
                        com.atlassian.plugin.web.api.descriptors.WebItemModuleDescriptor.class))
                .thenReturn(Lists.newArrayList(webItemDescriptor1, webItemDescriptor2, webItemDescriptor3));

        final WebSectionModuleDescriptor webSection1 =
                initWebSection(SAMPLE_SECTION, 0, "test section tooltip", "Test section 1", false);
        final WebSectionModuleDescriptor webSection2 = initWebSection(SAMPLE_SECTION, 10, null, "Test section 2", true);
        final WebSectionModuleDescriptor webSection3 =
                initWebSection(OTHER_SECTION, 10, "test section tooltip", "Test section 1", false);
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(
                        com.atlassian.plugin.web.api.descriptors.WebSectionModuleDescriptor.class))
                .thenReturn(Lists.newArrayList(webSection1, webSection2, webSection3));
    }

    @Test
    public void testDynamicWebItemsWithNoProviders() {
        final Iterable<WebItem> webItems =
                defaultWebInterfaceManager.getWebItems(SAMPLE_SECTION, Collections.emptyMap());

        assertThat(webItems, Matchers.<WebItem>contains(eqWebItem(STATIC_WEB_ITEM_1), eqWebItem(STATIC_WEB_ITEM_2)));
    }

    @Test
    public void testWebItemsWithNoLink() {
        WebItemModuleDescriptor webItem = initWebItem(
                SAMPLE_SECTION, 0, "/rest/api/1", "test-link", "a", "test link tooltip", "Test link 1", false);
        WebItemModuleDescriptor webItemNoLink =
                initWebItem(SAMPLE_SECTION, 0, null, "test-link", null, "test link tooltip", "Test link 1", false);
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(
                        com.atlassian.plugin.web.api.descriptors.WebItemModuleDescriptor.class))
                .thenReturn(Lists.newArrayList(webItem, webItemNoLink));

        final Iterable<WebItem> webItems =
                defaultWebInterfaceManager.getWebItems(SAMPLE_SECTION, Collections.emptyMap());

        final WebItem webItemWithNoLink = new WebFragmentBuilder(0)
                .styleClass("test-link")
                .title("test link tooltip")
                .label("Test link 1")
                .webItem(SAMPLE_SECTION)
                .build();
        assertThat(webItems, Matchers.<WebItem>contains(eqWebItem(STATIC_WEB_ITEM_1), eqWebItem(webItemWithNoLink)));
    }

    @Test
    public void testWebItemsWithNoAccessKey() {
        WebItemModuleDescriptor webItemNoAccessKey = initWebItem(
                SAMPLE_SECTION, 0, "/rest/api/1", "test-link", null, "test link tooltip", "Test link 1", false);
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(
                        com.atlassian.plugin.web.api.descriptors.WebItemModuleDescriptor.class))
                .thenReturn(Lists.newArrayList(webItemNoAccessKey));

        final Iterable<WebItem> webItems =
                defaultWebInterfaceManager.getWebItems(SAMPLE_SECTION, Collections.emptyMap());

        final WebItem webItemWithNoAccessKey = new WebFragmentBuilder(0)
                .styleClass("test-link")
                .title("test link tooltip")
                .label("Test link 1")
                .webItem(SAMPLE_SECTION)
                .url("/rest/api/1")
                .build();
        assertThat(webItems, Matchers.<WebItem>contains(eqWebItem(webItemWithNoAccessKey)));
    }

    @Test
    public void testDynamicWebItemsWithEmptyProviders() {
        final WebItemProviderModuleDescriptor webItemProviderModuleDescriptor =
                Mockito.mock(WebItemProviderModuleDescriptor.class);
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(WebItemProviderModuleDescriptor.class))
                .thenReturn(Lists.newArrayList(webItemProviderModuleDescriptor));

        when(webItemProviderModuleDescriptor.getModule()).thenReturn(getWebItemProvider());

        final Iterable<WebItem> webItems =
                defaultWebInterfaceManager.getWebItems(SAMPLE_SECTION, Collections.emptyMap());
        assertThat(webItems, Matchers.<WebItem>contains(eqWebItem(STATIC_WEB_ITEM_1), eqWebItem(STATIC_WEB_ITEM_2)));
    }

    @Test
    public void testDynamicWebItemsWithProviders() {
        final WebItemProviderModuleDescriptor descriptor = Mockito.mock(WebItemProviderModuleDescriptor.class);
        when(descriptor.getSection()).thenReturn(SAMPLE_SECTION);
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(WebItemProviderModuleDescriptor.class))
                .thenReturn(Lists.newArrayList(descriptor));

        when(descriptor.getModule()).thenReturn(getWebItemProvider(DYNAMIC_WEB_ITEM_1, DYNAMIC_WEB_ITEM_2));

        final Iterable<WebItem> webItems =
                defaultWebInterfaceManager.getWebItems(SAMPLE_SECTION, Collections.emptyMap());

        assertThat(
                webItems,
                Matchers.<WebItem>contains(
                        eqWebItem(STATIC_WEB_ITEM_1),
                        eqWebItem(DYNAMIC_WEB_ITEM_1),
                        eqWebItem(STATIC_WEB_ITEM_2),
                        eqWebItem(DYNAMIC_WEB_ITEM_2)));
    }

    @Test
    public void testDynamicWebItemsWithConditions() {
        final WebItemProviderModuleDescriptor webItemProviderModuleDescriptor =
                Mockito.mock(WebItemProviderModuleDescriptor.class);
        when(webItemProviderModuleDescriptor.getSection()).thenReturn(SAMPLE_SECTION);
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(WebItemProviderModuleDescriptor.class))
                .thenReturn(Lists.newArrayList(webItemProviderModuleDescriptor));

        when(webItemProviderModuleDescriptor.getModule())
                .thenReturn(getWebItemProvider(DYNAMIC_WEB_ITEM_1, DYNAMIC_WEB_ITEM_2));

        final Iterable<WebItem> webItems =
                defaultWebInterfaceManager.getDisplayableWebItems(SAMPLE_SECTION, Collections.emptyMap());

        assertThat(
                webItems,
                Matchers.<WebItem>contains(
                        eqWebItem(STATIC_WEB_ITEM_1), eqWebItem(DYNAMIC_WEB_ITEM_1), eqWebItem(DYNAMIC_WEB_ITEM_2)));
    }

    @Test
    public void testItemProviderErrors() {
        final WebItemProviderModuleDescriptor webItemProviderModuleDescriptor =
                Mockito.mock(WebItemProviderModuleDescriptor.class);
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(WebItemProviderModuleDescriptor.class))
                .thenReturn(Lists.newArrayList(webItemProviderModuleDescriptor));

        when(webItemProviderModuleDescriptor.getModule()).thenReturn(new WebItemProvider() {
            @Override
            public Iterable<WebItem> getItems(final Map<String, Object> context) {
                throw new RuntimeException("Something went wront in a plugin somewhere!!");
            }
        });

        final Iterable<WebItem> webItems =
                defaultWebInterfaceManager.getWebItems(SAMPLE_SECTION, Collections.emptyMap());

        assertThat(webItems, Matchers.<WebItem>contains(eqWebItem(STATIC_WEB_ITEM_1), eqWebItem(STATIC_WEB_ITEM_2)));
    }

    @Test
    public void testDynamicWebSectionsWithNoProviders() {
        final Iterable<WebSection> webSections =
                defaultWebInterfaceManager.getWebSections(SAMPLE_SECTION, Collections.emptyMap());

        assertThat(
                webSections,
                Matchers.<WebSection>contains(eqWebSection(STATIC_WEB_SECTION_1), eqWebSection(STATIC_WEB_SECTION_2)));
    }

    @Test
    public void testDynamicWebSectionsWithEmptyProviders() {
        final WebSectionProviderModuleDescriptor webSectionProviderModuleDescriptor =
                Mockito.mock(WebSectionProviderModuleDescriptor.class);
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(WebSectionProviderModuleDescriptor.class))
                .thenReturn(Lists.newArrayList(webSectionProviderModuleDescriptor));

        when(webSectionProviderModuleDescriptor.getModule()).thenReturn(getWebSectionProvider());

        final Iterable<WebSection> webSections =
                defaultWebInterfaceManager.getWebSections(SAMPLE_SECTION, Collections.emptyMap());

        assertThat(
                webSections,
                Matchers.<WebSection>contains(eqWebSection(STATIC_WEB_SECTION_1), eqWebSection(STATIC_WEB_SECTION_2)));
    }

    @Test
    public void testDynamicWebSectionsWithProviders() {
        final WebSectionProviderModuleDescriptor webSectionProviderModuleDescriptor =
                Mockito.mock(WebSectionProviderModuleDescriptor.class);
        when(webSectionProviderModuleDescriptor.getLocation()).thenReturn(SAMPLE_SECTION);
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(WebSectionProviderModuleDescriptor.class))
                .thenReturn(Lists.newArrayList(webSectionProviderModuleDescriptor));

        when(webSectionProviderModuleDescriptor.getModule())
                .thenReturn(getWebSectionProvider(DYNAMIC_WEB_SECTION_1, DYNAMIC_WEB_SECTION_2));

        final Iterable<WebSection> webSections =
                defaultWebInterfaceManager.getWebSections(SAMPLE_SECTION, Collections.emptyMap());

        assertThat(
                webSections,
                Matchers.<WebSection>contains(
                        eqWebSection(STATIC_WEB_SECTION_1),
                        eqWebSection(DYNAMIC_WEB_SECTION_1),
                        eqWebSection(STATIC_WEB_SECTION_2),
                        eqWebSection(DYNAMIC_WEB_SECTION_2)));
    }

    @Test
    public void testDynamicWebSectionsWithConditions() {
        final WebSectionProviderModuleDescriptor webSectionProviderModuleDescriptor =
                Mockito.mock(WebSectionProviderModuleDescriptor.class);
        when(webSectionProviderModuleDescriptor.getLocation()).thenReturn(SAMPLE_SECTION);
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(WebSectionProviderModuleDescriptor.class))
                .thenReturn(Lists.newArrayList(webSectionProviderModuleDescriptor));

        when(webSectionProviderModuleDescriptor.getModule())
                .thenReturn(getWebSectionProvider(DYNAMIC_WEB_SECTION_1, DYNAMIC_WEB_SECTION_2));

        final Iterable<WebSection> webSections =
                defaultWebInterfaceManager.getDisplayableWebSections(SAMPLE_SECTION, Collections.emptyMap());

        assertThat(
                webSections,
                Matchers.<WebSection>contains(
                        eqWebSection(STATIC_WEB_SECTION_1),
                        eqWebSection(DYNAMIC_WEB_SECTION_1),
                        eqWebSection(DYNAMIC_WEB_SECTION_2)));
    }

    @Test
    public void testSectionProviderErrors() {
        final WebSectionProviderModuleDescriptor webSectionProviderModuleDescriptor =
                Mockito.mock(WebSectionProviderModuleDescriptor.class);
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(WebSectionProviderModuleDescriptor.class))
                .thenReturn(Lists.newArrayList(webSectionProviderModuleDescriptor));

        when(webSectionProviderModuleDescriptor.getModule()).thenReturn(new WebSectionProvider() {
            @Override
            public Iterable<WebSection> getSections(final Map<String, Object> context) {
                throw new RuntimeException("Something went wront in a plugin somewhere!!");
            }
        });

        final Iterable<WebSection> webSections =
                defaultWebInterfaceManager.getWebSections(SAMPLE_SECTION, Collections.emptyMap());

        assertThat(
                webSections,
                Matchers.<WebSection>contains(eqWebSection(STATIC_WEB_SECTION_1), eqWebSection(STATIC_WEB_SECTION_2)));
    }

    private WebSectionProvider getWebSectionProvider(final WebSection... sections) {
        return new WebSectionProvider() {
            @Override
            public Iterable<WebSection> getSections(final Map<String, Object> context) {
                // test for null safe implementation in WebInterfaceManager
                if (sections == null) {
                    return null;
                }
                return Lists.newArrayList(sections);
            }
        };
    }

    private WebItemProvider getWebItemProvider(final WebItem... items) {
        return new WebItemProvider() {
            @Override
            public Iterable<WebItem> getItems(final Map<String, Object> context) {
                // test for null safe implementation in WebInterfaceManager
                if (items == null) {
                    return null;
                }
                return Lists.newArrayList(items);
            }
        };
    }

    private WebSectionModuleDescriptor initWebSection(
            String location, int weight, String tooltip, String label, boolean addCondition) {
        final WebSectionModuleDescriptor webSection = Mockito.mock(WebSectionModuleDescriptor.class);
        initWebFragment(webSection, weight, tooltip, label, addCondition);

        when(webSection.getLocation()).thenReturn(location);
        return webSection;
    }

    private WebItemModuleDescriptor initWebItem(
            String section,
            int weight,
            String linkUrl,
            String styleClass,
            String accessKey,
            String tooltip,
            String label,
            boolean addCondition) {
        final WebItemModuleDescriptor webItem = Mockito.mock(WebItemModuleDescriptor.class);
        initWebFragment(webItem, weight, tooltip, label, addCondition);

        when(webItem.getSection()).thenReturn(section);
        when(webItem.getStyleClass()).thenReturn(styleClass);

        if (linkUrl != null) {
            Element link = DocumentHelper.createElement("link");
            link.setText(linkUrl);
            if (accessKey != null) {
                link.addAttribute("accessKey", accessKey);
            }
            DefaultWebLink webLink = new DefaultWebLink(
                    new Dom4jDelegatingElement(link),
                    new MockWebFragmentHelper(),
                    Mockito.mock(ContextProvider.class),
                    Mockito.mock(WebFragmentModuleDescriptor.class));
            when(webItem.getLink()).thenReturn(webLink);
        }

        return webItem;
    }

    private void initWebFragment(
            WebFragmentModuleDescriptor fragment, int weight, String tooltip, String label, boolean addCondition) {
        when(fragment.getWeight()).thenReturn(weight);
        final WebLabel tooltipLabel = Mockito.mock(WebLabel.class);
        when(tooltipLabel.getDisplayableLabel(isNull(), anyMap())).thenReturn(tooltip);
        when(fragment.getTooltip()).thenReturn(tooltipLabel);

        final WebLabel webLabel = Mockito.mock(WebLabel.class);
        when(webLabel.getDisplayableLabel(isNull(), anyMap())).thenReturn(label);
        when(fragment.getWebLabel()).thenReturn(webLabel);

        if (addCondition) {
            when(fragment.getCondition()).thenReturn(new NeverDisplayCondition());
        }
    }
}
