package com.atlassian.plugin.web;

import java.util.Objects;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.junit.internal.matchers.TypeSafeMatcher;

import com.atlassian.plugin.web.api.WebSection;

/**
 * Deep matcher for {@link com.atlassian.plugin.web.api.WebSection}.
 */
public class EqWebSection extends TypeSafeMatcher<WebSection> {
    private final WebSection expected;

    public EqWebSection(final WebSection expected) {
        this.expected = expected;
    }

    @Override
    public boolean matchesSafely(WebSection item) {
        return Objects.equals(expected.getId(), item.getId())
                && Objects.equals(expected.getLocation(), item.getLocation())
                && Objects.equals(expected.getLabel(), item.getLabel())
                && Objects.equals(expected.getStyleClass(), item.getStyleClass())
                && Objects.equals(expected.getTitle(), item.getTitle())
                && expected.getWeight() == item.getWeight()
                && expected.getParams().equals(item.getParams());
    }

    public void describeTo(Description description) {
        description.appendText(
                "did not match " + ToStringBuilder.reflectionToString(expected, ToStringStyle.SHORT_PREFIX_STYLE));
    }

    @Factory
    public static Matcher<WebSection> eqWebSection(final WebSection expected) {
        return new EqWebSection(expected);
    }
}
