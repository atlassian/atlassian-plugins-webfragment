package com.atlassian.plugin.web.descriptors;

import com.atlassian.plugin.web.model.WebPanel;

/**
 * A test WebPanel that outputs the values passed in the context, or a filler
 * message if the context is empty.
 */
public class MockLegacyWebPanel extends MockWebPanel implements WebPanel {}
