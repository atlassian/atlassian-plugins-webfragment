package com.atlassian.plugin.web.descriptors;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.Writer;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import com.google.common.collect.ImmutableMap;

import junit.framework.TestCase;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginArtifact;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.hostcontainer.HostContainer;
import com.atlassian.plugin.impl.AbstractPlugin;
import com.atlassian.plugin.internal.module.Dom4jDelegatingElement;
import com.atlassian.plugin.module.Element;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.web.WebFragmentHelper;
import com.atlassian.plugin.web.WebInterfaceManager;
import com.atlassian.plugin.web.api.renderer.RendererException;
import com.atlassian.plugin.web.api.renderer.WebPanelRenderer;
import com.atlassian.plugin.web.impl.NoOpContextProvider;
import com.atlassian.plugin.web.model.EmbeddedTemplateWebPanel;
import com.atlassian.plugin.web.model.ResourceTemplateWebPanel;
import com.atlassian.plugin.web.model.WebPanel;
import com.atlassian.plugin.web.model.WebPanelTestUtils;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

public class TestDefaultWebPanelModuleDescriptor extends TestCase {
    private WebPanelModuleDescriptor descriptor;
    private final PluginArtifact pluginArtifact = mock(PluginArtifact.class);
    private final Plugin plugin = new MockPlugin(pluginArtifact, this.getClass().getName());
    private final HostContainer hostContainer = mock(HostContainer.class);
    private final WebInterfaceManager webInterfaceManager = spy(new MockWebInterfaceManager());
    private final ModuleFactory moduleClassFactory = mock(ModuleFactory.class);
    private final PluginAccessor pluginAccessor = mock(PluginAccessor.class);
    private final Map<String, Object> context = new HashMap<String, Object>();

    @Override
    protected void setUp() throws Exception {
        when(hostContainer.create(EmbeddedTemplateWebPanel.class))
                .thenReturn(new EmbeddedTemplateWebPanel(pluginAccessor));
        when(hostContainer.create(ResourceTemplateWebPanel.class))
                .thenReturn(new ResourceTemplateWebPanel(pluginAccessor));

        descriptor = new DefaultWebPanelModuleDescriptor(hostContainer, moduleClassFactory, webInterfaceManager);
    }

    public void testMissingResource() throws DocumentException, PluginParseException {
        final String webPanelXml = "<web-panel key=\"myPanel\" location=\"atl.header\"/>";
        try {
            descriptor.init(plugin, createElement(webPanelXml));
            fail("Descriptor should check for a resource with name 'view'");
        } catch (PluginParseException e) {
            // pass
        }
    }

    public void testMissingLocation() throws DocumentException, PluginParseException {
        final String webPanelXml = "<web-panel key=\"myPanel\">\n"
                + "  <resource name=\"view\" type=\"static\"><![CDATA[<b>Hello World!</b>]]></resource>\n"
                + "</web-panel>";
        try {
            descriptor.init(plugin, createElement(webPanelXml));
            fail("Descriptor should check for a location attribute");
        } catch (PluginParseException e) {
            // pass
        }
    }

    public void testGetEmbeddedTemplate() throws DocumentException, PluginParseException {
        final String webPanelXml = "<web-panel key=\"myPanel\" location=\"atl.header\">\n"
                + "  <resource name=\"view\" type=\"static\"><![CDATA[<b>Hello World!</b>]]></resource>\n"
                + "</web-panel>";
        descriptor.init(plugin, createElement(webPanelXml));
        descriptor.enabled();

        assertEquals("atl.header", descriptor.getLocation());
        assertContextProvider(NoOpContextProvider.class);
        assertEquals("<b>Hello World!</b>", descriptor.getModule().getHtml(context));
    }

    public void testGetEmbeddedTemplateWithContextProvider() throws DocumentException, PluginParseException {
        final String webPanelXml = "<web-panel key=\"myPanel\" location=\"atl.header\">\n"
                + "  <resource name=\"view\" type=\"velocity\"><![CDATA[$addedToContext]]></resource>\n"
                + "  <context-provider class=\"com.atlassian.plugin.web.descriptors.MockContextProvider\" />\n"
                + "</web-panel>";
        descriptor.init(plugin, createElement(webPanelXml));
        descriptor.enabled();

        final WebPanelRenderer renderer = new MockVelocityRenderer();
        WebPanelTestUtils.mockPluginAccessorReturning(pluginAccessor, renderer);

        HashMap<String, Object> originalContext = new HashMap<String, Object>();
        assertRendered(MockContextProvider.DATA_ADDED_TO_CONTEXT, originalContext);
        assertEquals(0, originalContext.size());
    }

    public void testGetResourceTemplate() throws DocumentException, PluginParseException {
        final String webPanelXml = "<web-panel key=\"myPanel\" location=\"atl.header\">\n"
                + "  <resource name=\"view\" type=\"static\" location=\"ResourceTemplateWebPanelTest.txt\"/>\n"
                + "</web-panel>";

        descriptor.init(plugin, createElement(webPanelXml));
        descriptor.enabled();

        assertContextProvider(NoOpContextProvider.class);

        String html = descriptor.getModule().getHtml(context);
        assertTrue(html.startsWith("This <file> is used as web panel contents in unit tests"));
    }

    public void testGetResourceTemplateWithContextProvider() throws DocumentException, PluginParseException {
        final String webPanelXml = "<web-panel key=\"myPanel\" location=\"atl.header\">\n"
                + "  <resource name=\"view\" type=\"velocity\" location=\"ResourceTemplateWebPanelTest.txt\"/>\n"
                + "  <context-provider class=\"com.atlassian.plugin.web.descriptors.MockContextProvider\" />\n"
                + "</web-panel>";
        descriptor.init(plugin, createElement(webPanelXml));
        descriptor.enabled();

        final WebPanelRenderer renderer = new MockVelocityRenderer();
        WebPanelTestUtils.mockPluginAccessorReturning(pluginAccessor, renderer);

        HashMap<String, Object> originalContext = new HashMap<String, Object>();
        assertRendered(MockContextProvider.DATA_ADDED_TO_CONTEXT, originalContext);
        assertEquals(0, originalContext.size());
    }

    public void testWebPanelClass() throws DocumentException, PluginParseException {
        when(moduleClassFactory.createModule(eq("com.atlassian.plugin.web.descriptors.MockWebPanel"), any()))
                .thenReturn(new MockWebPanel());

        final String webPanelXml =
                "<web-panel key=\"myPanel\" location=\"atl.header\" class=\"com.atlassian.plugin.web.descriptors.MockWebPanel\"/>";
        descriptor.init(plugin, createElement(webPanelXml));
        descriptor.enabled();

        assertRendered(MockWebPanel.NOTHING_IN_CONTEXT, new HashMap<String, Object>());
    }

    public void testWebPanelClassWithContextProvider() throws DocumentException, PluginParseException {
        when(moduleClassFactory.createModule(eq("com.atlassian.plugin.web.descriptors.MockWebPanel"), any()))
                .thenReturn(new MockWebPanel());

        final String webPanelXml =
                "<web-panel key=\"myPanel\" location=\"atl.header\" class=\"com.atlassian.plugin.web.descriptors.MockWebPanel\">\n"
                        + "  <context-provider class=\"com.atlassian.plugin.web.descriptors.MockContextProvider\" />\n"
                        + "</web-panel>";
        descriptor.init(plugin, createElement(webPanelXml));
        descriptor.enabled();

        HashMap<String, Object> originalContext = new HashMap<String, Object>();
        assertRendered(MockContextProvider.DATA_ADDED_TO_CONTEXT, originalContext);

        // Confirm that the original context was protected from the ContextProvider's changes
        assertEquals(0, originalContext.size());
    }

    public void testSimpleLabelTemplate() throws DocumentException, PluginParseException {
        WebFragmentHelper fragmentHelper = spy(webInterfaceManager.getWebFragmentHelper());
        when(webInterfaceManager.getWebFragmentHelper()).thenReturn(fragmentHelper);

        Map<String, Object> ctx = ImmutableMap.of("x", "foo");

        when(fragmentHelper.renderVelocityFragment("Hello ${x}", ctx)).thenReturn("It Worked!");

        final String webPanelXml = "<web-panel key=\"myPanel\" location=\"atl.header\">\n"
                + "  <resource name=\"view\" type=\"static\"><![CDATA[<b>Hello World!</b>]]></resource>\n"
                + "  <label>Hello ${x}</label>"
                + "</web-panel>";
        descriptor.init(plugin, createElement(webPanelXml));
        descriptor.enabled();

        assertContextProvider(NoOpContextProvider.class);
        assertEquals("It Worked!", descriptor.getWebLabel().getDisplayableLabel(null, ctx));
    }

    private void assertRendered(String expectedHtml, Map<String, Object> originalContext) {
        WebPanel webPanel = descriptor.getModule();
        String renderedHtml = webPanel.getHtml(originalContext);
        assertEquals(expectedHtml, renderedHtml);
    }

    private void assertContextProvider(Class expectedContextProviderClass) {
        assertEquals(
                expectedContextProviderClass, descriptor.getContextProvider().getClass());
    }

    private Element createElement(final String childElement) throws DocumentException {
        final Document document = DocumentHelper.parseText(childElement);
        return new Dom4jDelegatingElement(document.getRootElement());
    }

    private class MockPlugin extends AbstractPlugin {
        MockPlugin(final PluginArtifact pluginArtifact, final String key) {
            super(pluginArtifact);
            setKey(key);
            setName(key);
        }

        public boolean isUninstallable() {
            return false;
        }

        public boolean isDeleteable() {
            return false;
        }

        public boolean isDynamicallyLoaded() {
            return false;
        }

        public <T> Class<T> loadClass(final String clazz, final Class<?> callingClass) throws ClassNotFoundException {
            return null;
        }

        public ClassLoader getClassLoader() {
            return this.getClass().getClassLoader();
        }

        public URL getResource(final String path) {
            return null;
        }

        public InputStream getResourceAsStream(final String name) {
            return null;
        }
    }

    private class MockVelocityRenderer implements WebPanelRenderer {
        static final String NOTHING_IN_CONTEXT = "nothing in context";

        public String getResourceType() {
            return "velocity";
        }

        public void render(String templateName, Plugin plugin, Map<String, Object> context, Writer writer)
                throws RendererException, IOException {
            if (!context.isEmpty()) {
                for (Object value : context.values()) {
                    writer.append(value.toString());
                }
            } else {
                writer.append(NOTHING_IN_CONTEXT);
            }
        }

        public String renderFragment(String fragment, Plugin plugin, Map<String, Object> context)
                throws RendererException {
            try {
                StringWriter out = new StringWriter();
                renderFragment(out, fragment, plugin, context);
                return out.toString();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        @Override
        public void renderFragment(Writer writer, String fragment, Plugin plugin, Map<String, Object> context)
                throws RendererException, IOException {
            if (!context.isEmpty()) {
                for (Object value : context.values()) {
                    writer.write(value.toString());
                }
            } else {
                writer.write(NOTHING_IN_CONTEXT);
            }
        }
    }
}
