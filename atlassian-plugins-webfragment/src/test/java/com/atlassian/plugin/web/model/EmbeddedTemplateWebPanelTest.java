package com.atlassian.plugin.web.model;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Collections;
import java.util.Map;

import junit.framework.TestCase;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.web.api.renderer.RendererException;
import com.atlassian.plugin.web.api.renderer.WebPanelRenderer;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.ArgumentMatchers.same;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class EmbeddedTemplateWebPanelTest extends TestCase {
    private PluginAccessor accessorMock;
    private WebPanelRenderer rendererMock;
    private final Map<String, Object> emptyContext = Collections.emptyMap();

    @Override
    protected void setUp() throws Exception {
        accessorMock = mock(PluginAccessor.class);
        rendererMock = mock(WebPanelRenderer.class);
    }

    @Override
    protected void tearDown() throws Exception {
        accessorMock = null;
        rendererMock = null;
    }

    public void testWriteHtml() throws IOException {
        EmbeddedTemplateWebPanel webPanel = newEmbeddedTemplateWebPanel(accessorMock, "static");

        StringWriter out = new StringWriter();
        webPanel.writeHtml(out, Collections.emptyMap());
        assertEquals("<body>", out.toString());
    }

    public void testWriteHtmlErrorMessagesEscaped() throws IOException {
        setMocksToThrowException(accessorMock, rendererMock);

        EmbeddedTemplateWebPanel webPanel = newEmbeddedTemplateWebPanel(accessorMock, "cheese");

        StringWriter out = new StringWriter();
        webPanel.writeHtml(out, emptyContext);
        assertEquals("Error rendering WebPanel: &lt;cheese&gt;\nTemplate contents: &lt;body&gt;", out.toString());
    }

    public void testGetHtml() {
        EmbeddedTemplateWebPanel webPanel = newEmbeddedTemplateWebPanel(accessorMock, "static");

        assertEquals("<body>", webPanel.getHtml(Collections.emptyMap()));
    }

    public void testErrorMessagesEscaped() throws IOException {
        setMocksToThrowException(accessorMock, rendererMock);

        EmbeddedTemplateWebPanel webPanel = newEmbeddedTemplateWebPanel(accessorMock, "cheese");

        assertEquals(
                "Error rendering WebPanel: &lt;cheese&gt;\nTemplate contents: &lt;body&gt;",
                webPanel.getHtml(emptyContext));
    }

    private void setMocksToThrowException(PluginAccessor accessorMock, WebPanelRenderer rendererMock)
            throws IOException {
        WebPanelTestUtils.mockPluginAccessorReturning(accessorMock, rendererMock);

        when(rendererMock.getResourceType()).thenReturn("cheese");
        doThrow(new RendererException("<cheese>"))
                .when(rendererMock)
                .renderFragment(isA(Writer.class), eq("<body>"), (Plugin) isNull(), same(emptyContext));
    }

    public void testUnsupportedResourceType() {
        when(rendererMock.getResourceType()).thenReturn("velocity");
        WebPanelTestUtils.mockPluginAccessorReturning(accessorMock, rendererMock);

        EmbeddedTemplateWebPanel webPanel = newEmbeddedTemplateWebPanel(accessorMock, "unsupported-type");

        String result = webPanel.getHtml(emptyContext);
        assertNotNull(result);
        assertTrue(result.toLowerCase().contains("error"));
    }

    private EmbeddedTemplateWebPanel newEmbeddedTemplateWebPanel(PluginAccessor accessorMock, String resourceType) {
        EmbeddedTemplateWebPanel webPanel = new EmbeddedTemplateWebPanel(accessorMock);
        webPanel.setResourceType(resourceType);
        webPanel.setTemplateBody("<body>");
        return webPanel;
    }
}
