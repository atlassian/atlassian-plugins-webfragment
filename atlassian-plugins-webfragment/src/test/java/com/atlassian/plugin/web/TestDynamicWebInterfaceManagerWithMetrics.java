package com.atlassian.plugin.web;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.assertj.core.api.Assertions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import com.google.common.collect.Lists;
import io.micrometer.core.instrument.Metrics;
import io.micrometer.core.instrument.Timer;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;

import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.web.api.WebItem;
import com.atlassian.plugin.web.api.WebSection;
import com.atlassian.plugin.web.api.descriptors.WebPanelModuleDescriptor;
import com.atlassian.plugin.web.descriptors.DefaultWebPanelModuleDescriptor;
import com.atlassian.plugin.web.descriptors.WebItemModuleDescriptor;
import com.atlassian.plugin.web.descriptors.WebSectionModuleDescriptor;
import com.atlassian.plugin.web.impl.DefaultWebInterfaceManager;
import com.atlassian.plugin.web.model.WebPanel;
import com.atlassian.util.profiling.StrategiesRegistry;
import com.atlassian.util.profiling.micrometer.MicrometerStrategy;
import com.atlassian.util.profiling.strategy.MetricStrategy;

import static java.lang.Thread.sleep;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import static com.atlassian.plugin.web.impl.DefaultWebInterfaceManager.CONDITION_CLASSNAME_TAG_KEY;
import static com.atlassian.plugin.web.impl.DefaultWebInterfaceManager.CONDITION_METRIC_KEY;
import static com.atlassian.plugin.web.impl.DefaultWebInterfaceManager.FRAGMENT_LOCATION_TAG_KEY;

@RunWith(MockitoJUnitRunner.class)
public class TestDynamicWebInterfaceManagerWithMetrics {
    private static final String SAMPLE_SECTION = "top.nav.sample";
    private static final long CONDITION_SLEEP_TIME_IN_MILLIS = 150L;

    @Mock
    private PluginAccessor pluginAccessor;

    @Mock
    private WebFragmentHelper webFragmentHelper;

    @InjectMocks
    private DefaultWebInterfaceManager defaultWebInterfaceManager;

    private SimpleMeterRegistry registry;

    @Before
    public void setUp() {
        Metrics.globalRegistry.clear();
        registry = new SimpleMeterRegistry();
        com.atlassian.util.profiling.Metrics.getConfiguration().setEnabled(true);
        MetricStrategy strategy = new MicrometerStrategy(registry);
        StrategiesRegistry.addMetricStrategy(strategy);
        Metrics.addRegistry(registry);
    }

    @After
    public void afterEach() {
        Metrics.removeRegistry(registry);
    }

    @Test
    public void shouldEmitMetrics_whenConditionShouldDisplayIsCalled() {
        final WebItemModuleDescriptor webItemDescriptor1 = initWebItem();
        final WebItemModuleDescriptor webItemDescriptor2 = initWebItem();
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(
                        com.atlassian.plugin.web.api.descriptors.WebItemModuleDescriptor.class))
                .thenReturn(Lists.newArrayList(webItemDescriptor1, webItemDescriptor2));

        final Iterable<WebItem> webItems =
                defaultWebInterfaceManager.getDisplayableWebItems(SAMPLE_SECTION, Collections.emptyMap());
        assertThat(webItems).hasSize(2);

        assertMetricHasBeenEmitted();
    }

    @Test
    public void getDisplayableWebPanelDescriptors_shouldEmitMetrics_whenConditionShouldDisplayIsCalled() {
        final DefaultWebPanelModuleDescriptor webPanel1 = initWebPanel();
        final DefaultWebPanelModuleDescriptor webPanel2 = initWebPanel();
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(WebPanelModuleDescriptor.class))
                .thenReturn(Lists.newArrayList(webPanel1, webPanel2));

        final List<WebPanel> webItems =
                defaultWebInterfaceManager.getDisplayableWebPanels(SAMPLE_SECTION, Collections.emptyMap());
        assertThat(webItems).hasSize(2);

        assertMetricHasBeenEmitted();
    }

    @Test
    public void getDisplayableWebSections_shouldEmitMetrics_whenConditionShouldDisplayIsCalled() {
        final WebSectionModuleDescriptor webSection1 = initWebSection();
        final WebSectionModuleDescriptor webSection2 = initWebSection();
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(
                        com.atlassian.plugin.web.api.descriptors.WebSectionModuleDescriptor.class))
                .thenReturn(Lists.newArrayList(webSection1, webSection2));

        final Iterable<WebSection> webSections =
                defaultWebInterfaceManager.getDisplayableWebSections(SAMPLE_SECTION, Collections.emptyMap());

        assertThat(webSections).hasSize(2);
        assertMetricHasBeenEmitted();
    }

    @Test
    public void getDisplayableWebItems_shouldNotEmitMetrics_whenThereAreNoConditions() {
        final Iterable<WebItem> webSections =
                defaultWebInterfaceManager.getDisplayableWebItems(SAMPLE_SECTION, Collections.emptyMap());
        assertThat(webSections).hasSize(0);

        Timer metricTimer = registry.find(CONDITION_METRIC_KEY).timer();
        assertThat(metricTimer).describedAs("should not emit metric").isNull();
    }

    private void assertMetricHasBeenEmitted() {
        Timer metricTimer = registry.find(CONDITION_METRIC_KEY)
                .tag(CONDITION_CLASSNAME_TAG_KEY, SlowCondition.class.getName())
                .tag(FRAGMENT_LOCATION_TAG_KEY, SAMPLE_SECTION)
                .timer();
        assertThat(metricTimer)
                .describedAs("A metric timer should have been started")
                .isNotNull();
        Assertions.assertThat((long) metricTimer.mean(MILLISECONDS))
                .describedAs("should capture duration of Condition#shouldDisplay() method")
                .isBetween(CONDITION_SLEEP_TIME_IN_MILLIS, CONDITION_SLEEP_TIME_IN_MILLIS + 20L);

        Assertions.assertThat(metricTimer.count())
                .describedAs("should capture number of Condition#shouldDisplay() invocations")
                .isEqualTo(2);
    }

    private WebItemModuleDescriptor initWebItem() {
        final WebItemModuleDescriptor webItem = mock(WebItemModuleDescriptor.class);

        when(webItem.getCondition()).thenReturn(new SlowCondition());
        when(webItem.getSection()).thenReturn(SAMPLE_SECTION);
        when(webItem.getStyleClass()).thenReturn("test-link");

        return webItem;
    }

    private DefaultWebPanelModuleDescriptor initWebPanel() {
        final DefaultWebPanelModuleDescriptor webPanel = Mockito.mock(DefaultWebPanelModuleDescriptor.class);
        when(webPanel.getLocation()).thenReturn(SAMPLE_SECTION);
        when(webPanel.getCondition()).thenReturn(new SlowCondition());

        return webPanel;
    }

    private WebSectionModuleDescriptor initWebSection() {
        final WebSectionModuleDescriptor webSectionModuleDescriptor = Mockito.mock(WebSectionModuleDescriptor.class);
        when(webSectionModuleDescriptor.getLocation()).thenReturn(SAMPLE_SECTION);
        when(webSectionModuleDescriptor.getCondition()).thenReturn(new SlowCondition());

        return webSectionModuleDescriptor;
    }

    private static class SlowCondition implements Condition {
        public void init(Map<String, String> params) {}

        public boolean shouldDisplay(Map<String, Object> context) {
            try {
                sleep(CONDITION_SLEEP_TIME_IN_MILLIS);
            } catch (InterruptedException ignored) {
            }
            return true;
        }
    }
}
