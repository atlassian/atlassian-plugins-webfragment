package com.atlassian.plugin.web.api.descriptors;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.StateAware;
import com.atlassian.plugin.web.api.renderer.WebPanelRenderer;

/**
 * @since 6.1.0
 */
public interface WebPanelRendererModuleDescriptor<T extends WebPanelRenderer> extends ModuleDescriptor<T>, StateAware {}
