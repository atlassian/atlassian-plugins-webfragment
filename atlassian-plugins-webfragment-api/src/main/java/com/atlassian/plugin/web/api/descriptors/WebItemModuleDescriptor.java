package com.atlassian.plugin.web.api.descriptors;

/**
 * A web-item plugin adds extra links to a particular section.
 *
 * @see WebSectionModuleDescriptor
 * @since 6.1.0
 */
public interface WebItemModuleDescriptor extends WebFragmentModuleDescriptor<Void> {
    String getSection();

    /**
     * Returns the item style as a "class" String consisting of one or more style classes. The default value returned
     * should be an empty String rather than null.
     * <p>
     * Where possible, use of this method is preferred over <code>getIcon</code> as it allows more flexibility for
     * CSS-based web element styling and class-based JavaScript behaviour.
     *
     * @return space-separated list of style classes
     */
    String getStyleClass();

    /**
     * @return The entry point web-resource key.
     * @since 5.1
     */
    String getEntryPoint();
}
