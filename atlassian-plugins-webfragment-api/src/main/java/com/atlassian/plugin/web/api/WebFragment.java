package com.atlassian.plugin.web.api;

import java.util.Map;
import javax.annotation.Nonnull;

import com.atlassian.annotations.PublicApi;

/**
 * Represents an abstract UI element with a number of commonly used properties.
 * <p>
 * Attributes returned by getters will not be escaped yet for whatever context they are inserted in. In other words if
 * are accessing Webfragments to be included in a HTML page, properties returned by getters should be appropriately HTML
 * escaped.
 *
 * @since v3.0.2
 */
@PublicApi
public interface WebFragment {

    /**
     * @return the complete underlying web-fragment plugin key
     */
    String getCompleteKey();

    /**
     * @return The label for the fragment
     */
    String getLabel();

    /**
     * @return The title (tooltip) for the fragment
     */
    String getTitle();

    /**
     * @return The style to apply to the fragment
     */
    String getStyleClass();

    /**
     * @return The unique id for the fragment.
     */
    String getId();

    /**
     * @return untyped params of this fragment
     */
    @Nonnull
    Map<String, String> getParams();

    /**
     * @return The weight for the fragment
     */
    int getWeight();
}
