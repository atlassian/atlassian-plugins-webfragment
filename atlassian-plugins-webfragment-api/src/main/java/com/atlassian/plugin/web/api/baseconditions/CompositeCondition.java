package com.atlassian.plugin.web.api.baseconditions;

/**
 * Interface for composite conditions
 *
 * @since 6.1.0
 */
public interface CompositeCondition<T extends BaseCondition> {
    void addCondition(T condition);
}
