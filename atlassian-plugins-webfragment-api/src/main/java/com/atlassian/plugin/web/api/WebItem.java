package com.atlassian.plugin.web.api;

import javax.annotation.Nonnull;

import com.atlassian.annotations.PublicApi;

/**
 * Represents a link in a particular section
 *
 * @since v3.0.2
 */
@PublicApi
public interface WebItem extends WebFragment {
    /**
     * @return the section that this web item should be displayed in.
     */
    @Nonnull
    String getSection();

    /**
     * @return The URL that the link points to. This should never be null.
     */
    @Nonnull
    String getUrl();

    /**
     * @return The access key used to quickly select link
     */
    String getAccessKey();

    /**
     * @return The entry point web-resource key.
     * @since 5.1
     */
    String getEntryPoint();
}
