package com.atlassian.plugin.web.api;

import javax.annotation.Nonnull;

import com.atlassian.annotations.PublicApi;

/**
 * Represents a logical grouping of various other {@link WebSection}s or {@link WebItem}s in a specific location.
 *
 * @since v3.0.2
 */
@PublicApi
public interface WebSection extends WebFragment {
    /**
     * @return the location a webSection belongs to
     */
    @Nonnull
    String getLocation();
}
