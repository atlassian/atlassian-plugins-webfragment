package com.atlassian.plugin.web;

import java.util.Map;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.api.baseconditions.BaseCondition;

/**
 * Decides whether a web section or web item should be displayed
 */
public interface Condition extends BaseCondition {
    /**
     * Called after creation and autowiring.
     * <p>
     * Default implementation since 7.2.0
     *
     * @param params The optional map of parameters specified in XML.
     */
    default void init(Map<String, String> params) throws PluginParseException {
        // do nothing
    }

    /**
     * Determine whether the web fragment should be displayed
     *
     * @return true if the user should see the fragment, false otherwise
     */
    boolean shouldDisplay(Map<String, Object> context);
}
