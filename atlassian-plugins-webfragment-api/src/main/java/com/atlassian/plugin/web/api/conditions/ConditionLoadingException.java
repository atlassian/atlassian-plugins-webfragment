package com.atlassian.plugin.web.api.conditions;

public class ConditionLoadingException extends Exception {
    public ConditionLoadingException() {}

    public ConditionLoadingException(String string) {
        super(string);
    }

    public ConditionLoadingException(String string, Throwable throwable) {
        super(string, throwable);
    }

    public ConditionLoadingException(Throwable throwable) {
        super(throwable);
    }
}
