package com.atlassian.plugin.web.api;

import java.util.Map;

import com.atlassian.plugin.web.WebInterfaceManager;

/**
 * <p> A dynamic web interface manager extends the existing {@link com.atlassian.plugin.web.WebInterfaceManager}
 * interface with methods to retrieve items and sections that are created by {@link
 * com.atlassian.plugin.web.api.provider.WebItemProvider}s and {@link com.atlassian.plugin.web.api.provider.WebSectionProvider}s
 * as well as static items and sections returned by the existing WebInterfaceManager. </p> <p> Host applications and plugins should
 * use these methods in favour of the WebInterfaceManager to retrieve pluggable items and sections since it allows for
 * greater flexibility when writing plugins. </p>
 *
 * @since v3.0.2
 */
public interface DynamicWebInterfaceManager extends WebInterfaceManager {
    /**
     * Returns a collection of web-items for the provided section. This collection is made up of both static web-items
     * defined in atlassian-plugin.xmls as well as dynamic items provided by {@link
     * com.atlassian.plugin.web.api.provider.WebItemProvider}s. For static items, no conditions will be applied.
     * <p>
     * At a minimum, context should provide a "request" {@code javax.servlet.http.HttpServletRequest} object to aid with
     * rendering of links including contextpath.
     *
     * @param section the section in which web-items should be located
     * @param context context to render urls, tooltips etc
     * @return a collection of web-items for the provided section
     */
    Iterable<WebItem> getWebItems(final String section, final Map<String, Object> context);

    /**
     * Same as {@link #getWebItems(String, java.util.Map)} but apply conditions for static web-items using the context
     * provided.
     * <p>
     * At a minimum, context should provide a "request" {@code javax.servlet.http.HttpServletRequest} object to aid with
     * rendering of links including contextpath.
     */
    Iterable<WebItem> getDisplayableWebItems(final String section, final Map<String, Object> context);

    /**
     * Returns a collection of web-sections for the provided location. This collection is made up of both static
     * web-sections defined in atlassian-plugin.xmls as well as dynamic sections provided by {@link
     * com.atlassian.plugin.web.api.provider.WebSectionProvider}s. For static sections, no conditions will be applied.
     * <p>
     * At a minimum, context should provide a "request" {@code javax.servlet.http.HttpServletRequest} object to aid with
     * rendering of links including contextpath.
     *
     * @param location the location in which web-sections should be located
     * @param context  context to render urls, tooltips etc
     * @return a collection of web-sections for the provided section
     */
    Iterable<WebSection> getWebSections(final String location, final Map<String, Object> context);

    /**
     * Same as {@link #getWebSections(String, java.util.Map)} but apply conditions for static web-sections using the
     * context provided.
     * <p>
     * At a minimum, context should provide a "request" {@code javax.servlet.http.HttpServletRequest} object to aid with
     * rendering of links including contextpath.
     */
    Iterable<WebSection> getDisplayableWebSections(final String location, final Map<String, Object> context);
}
