package com.atlassian.plugin.web.api.model;

import java.io.IOException;
import java.io.Writer;
import java.util.Map;

/**
 * The module that is responsive for providing the raw content for a Web Panel. Whatever is returned by
 * {@link #getHtml(Map)} is inserted into the host application's page, so it has to be valid HTML.
 * <p>
 * See {@code com.atlassian.plugin.web.descriptors.DefaultWebPanelModuleDescriptor#getModule()}
 *
 * @since 6.1.0
 */
public interface WebPanel {
    /**
     * Returns the HTML that will be placed in the host application's page.
     *
     * @param context the contextual information that can be used during rendering. Context elements are not
     *                standardized and are application-specific, so refer to your application's documentation to learn
     *                what is available.
     * @return the HTML that will be placed in the host application's page.
     */
    String getHtml(Map<String, Object> context);

    /**
     * Writes the HTML for this panel into the supplied writer. This method should be preferred over {@link #getHtml}
     * for large panels or for applications that make frequent use of panels, to avoid creating a lot of large garbage
     * buffer strings.
     *
     * @param writer  the writer to append the panel output to
     * @param context the contextual information that can be used during rendering. Context elements are not
     *                standardized and are application-specific, so refer to your application's documentation to learn
     *                what is available.
     * @throws IOException if there is some problem writing to the supplied writer
     */
    void writeHtml(Writer writer, Map<String, Object> context) throws IOException;
}
