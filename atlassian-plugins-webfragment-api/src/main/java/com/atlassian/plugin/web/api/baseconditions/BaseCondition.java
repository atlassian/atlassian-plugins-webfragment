package com.atlassian.plugin.web.api.baseconditions;

/**
 * Marker interface for conditions
 *
 * @since 6.1.0
 */
public interface BaseCondition {}
