package com.atlassian.plugin.web.api.provider;

import java.util.Map;
import javax.annotation.Nonnull;

import com.atlassian.plugin.web.api.WebItem;
import com.atlassian.plugin.web.api.descriptors.WebItemProviderModuleDescriptor;

/**
 * Allows plugins to provide dynamic web-items for a section and given context. This is useful when items should be
 * inserted in the UI based on data, such as a list of recent issues in a drop-down for example.
 *
 * @since v3.0.2
 */
public interface WebItemProvider {

    /**
     * Called after construction, but before {@link #getItems(Map)}
     *
     * @param moduleDescriptor for this instance of the {@link WebItemProvider}
     * @since 7.2.0
     */
    default void init(@Nonnull WebItemProviderModuleDescriptor<WebItemProvider> moduleDescriptor) {
        // do nothing
    }

    /**
     * Returns a list of web-items for a given context. May be null.
     *
     * @return a list of web-items for a given context. May be null.
     */
    Iterable<WebItem> getItems(final Map<String, Object> context);
}
