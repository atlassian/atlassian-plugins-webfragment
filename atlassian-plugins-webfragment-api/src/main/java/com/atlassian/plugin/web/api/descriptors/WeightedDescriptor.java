package com.atlassian.plugin.web.api.descriptors;

/**
 * A simple interface implemented by any weighted descriptors.
 *
 * @since 6.1.0
 */
public interface WeightedDescriptor {
    int getWeight();
}
