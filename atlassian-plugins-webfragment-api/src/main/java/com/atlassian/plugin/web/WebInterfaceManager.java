package com.atlassian.plugin.web;

import java.util.List;
import java.util.Map;

import com.atlassian.plugin.web.api.DynamicWebInterfaceManager;
import com.atlassian.plugin.web.api.descriptors.WebItemModuleDescriptor;
import com.atlassian.plugin.web.api.descriptors.WebPanelModuleDescriptor;
import com.atlassian.plugin.web.api.descriptors.WebSectionModuleDescriptor;
import com.atlassian.plugin.web.api.model.WebPanel;
import com.atlassian.plugin.web.api.provider.WebItemProvider;
import com.atlassian.plugin.web.api.provider.WebSectionProvider;

/**
 * A simple manager to provide sections of the web interface through plugins.
 * @deprecated Use {@link DynamicWebInterfaceManager}
 */
public interface WebInterfaceManager {
    /**
     * @return True if there are any sections for the given location.
     */
    boolean hasSectionsForLocation(String location);

    /**
     * @return A list of all WebSectionModuleDescriptors for the given location.
     * @deprecated as of 3.0.2 use {@link DynamicWebInterfaceManager#getWebSections(String, java.util.Map)}. Will be
     * removed in the next major version. The problem with this is it doesn't include {@link WebSectionProvider}s
     */
    List<? extends WebSectionModuleDescriptor> getSections(String location);

    /**
     * @return A list of all AbstractWebLinkFragmentModuleDescriptor <i>viewable in a given context</i> in the given
     * location.
     * @deprecated as of 3.0.2 use {@link DynamicWebInterfaceManager#getDisplayableWebSections(String, java.util.Map)}.
     * Will be removed in the next major version. The problem with this is it doesn't include
     * {@link WebSectionProvider}s
     */
    List<? extends WebSectionModuleDescriptor> getDisplayableSections(String location, Map<String, Object> context);

    /**
     * @return A list of all WebItemModuleDescriptors for the given section.
     * @deprecated as of 3.0.2 use {@link DynamicWebInterfaceManager#getWebItems(String, java.util.Map)}. Will be
     *  removed in the next major version. The problem with this is it doesn't include {@link WebItemProvider}s
     */
    List<? extends WebItemModuleDescriptor> getItems(String section);

    /**
     * @return A list of all AbstractWebLinkFragmentModuleDescriptor <i>viewable in a given context</i> in the given section.
     * @deprecated as of 3.0.2 use {@link DynamicWebInterfaceManager#getDisplayableWebItems(String, java.util.Map)}.
     * Will be removed in the next major version. The problem with this is it doesn't include {@link WebItemProvider}s
     */
    List<? extends WebItemModuleDescriptor> getDisplayableItems(String section, Map<String, Object> context);

    /**
     * @param location
     * @return A list of all {@link WebPanel} module instances
     * for the given location.
     */
    List<? extends WebPanel> getWebPanels(String location);

    /**
     * @param location
     * @param context
     * @return A list of all {@link WebPanel} module instances
     * <i>viewable in a given context</i> in the given location.
     */
    List<? extends WebPanel> getDisplayableWebPanels(String location, Map<String, Object> context);

    /**
     * @param location
     * @return A list of all {@link WebPanelModuleDescriptor} module instances
     * for the given location.
     */
    List<? extends WebPanelModuleDescriptor<? extends WebPanel>> getWebPanelDescriptors(String location);

    /**
     * @param location
     * @param context
     * @return A list of all {@link WebPanelModuleDescriptor} module instances
     * <i>viewable in a given context</i> in the given location.
     */
    List<? extends WebPanelModuleDescriptor<? extends WebPanel>> getDisplayableWebPanelDescriptors(
            String location, Map<String, Object> context);

    /**
     * Refresh the contents of the web interface manager.
     * @deprecated since 7.0 - Will be removed in the next major version. If you need to call this, there's either a bug
     * in the {@link WebInterfaceManager} implementation or a use-case we haven't considered, please create a ticket in
     * the <a href="https://ecosystem.atlassian.net/jira/software/c/projects/PLUGFRAG/issues">PLUGFRAG Jira project</a>
     */
    void refresh();

    /**
     * @return The web fragment helper for this implementation.
     */
    WebFragmentHelper getWebFragmentHelper();
}
