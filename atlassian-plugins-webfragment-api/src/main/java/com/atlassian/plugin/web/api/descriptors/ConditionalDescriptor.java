package com.atlassian.plugin.web.api.descriptors;

import com.atlassian.plugin.web.Condition;

/**
 * A simple interface implemented by any descriptors that support display conditions.
 *
 * @since 6.1.0
 */
public interface ConditionalDescriptor {
    Condition getCondition();
}
