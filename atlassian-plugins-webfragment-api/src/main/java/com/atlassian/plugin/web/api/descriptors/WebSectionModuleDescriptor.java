package com.atlassian.plugin.web.api.descriptors;

/**
 * A web-section plugin adds extra sections to a particular location.
 *
 * @since 6.1.0
 */
public interface WebSectionModuleDescriptor extends WebFragmentModuleDescriptor<Void> {
    String getLocation();
}
