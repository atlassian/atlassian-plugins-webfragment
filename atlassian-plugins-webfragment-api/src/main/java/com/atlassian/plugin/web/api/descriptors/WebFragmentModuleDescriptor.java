package com.atlassian.plugin.web.api.descriptors;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.StateAware;
import com.atlassian.plugin.web.Condition;

/**
 * A convenience interface for web fragment descriptors
 *
 * @since 6.1.0
 */
public interface WebFragmentModuleDescriptor<T>
        extends ModuleDescriptor<T>, WeightedDescriptor, StateAware, ContextAware, ConditionalDescriptor {
    int getWeight();

    Condition getCondition();
}
