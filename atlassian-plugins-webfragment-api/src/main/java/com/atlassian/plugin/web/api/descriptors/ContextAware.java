package com.atlassian.plugin.web.api.descriptors;

import com.atlassian.plugin.web.ContextProvider;

/**
 * Makes a plugin module aware of its Velocity context. Web modules should implement this interface if they want to
 * modify the Velocity context used to render their content.
 *
 * @since 6.1.0
 * @deprecated since 6.1.0 - Use dependency injection to get what's required directly from services where possible
 */
public interface ContextAware {
    /**
     * Returns the ContextProvider that augments the context used to render a web module.
     *
     * @deprecated Use dependency injection instead to get what's required
     */
    ContextProvider getContextProvider();
}
