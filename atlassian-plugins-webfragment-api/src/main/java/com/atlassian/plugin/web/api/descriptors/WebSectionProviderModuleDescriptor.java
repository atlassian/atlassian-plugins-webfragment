package com.atlassian.plugin.web.api.descriptors;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.StateAware;

public interface WebSectionProviderModuleDescriptor<WebSectionProvider>
        extends ModuleDescriptor<WebSectionProvider>, StateAware {

    /**
     * @return the location where the web-sections will appear
     * @since 7.2.0
     */
    default String getLocation() {
        return null;
    }
}
