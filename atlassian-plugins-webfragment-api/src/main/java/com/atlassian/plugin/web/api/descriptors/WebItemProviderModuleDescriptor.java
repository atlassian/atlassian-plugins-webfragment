package com.atlassian.plugin.web.api.descriptors;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.StateAware;

public interface WebItemProviderModuleDescriptor<WebItemProvider>
        extends ModuleDescriptor<WebItemProvider>, StateAware {

    /**
     * @return the section where the web-items will appear
     * @since 7.2.0
     */
    default String getSection() {
        return null;
    }
}
