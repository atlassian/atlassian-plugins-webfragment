package com.atlassian.plugin.web.api.provider;

import java.util.Map;
import javax.annotation.Nonnull;

import com.atlassian.plugin.web.api.WebSection;
import com.atlassian.plugin.web.api.descriptors.WebSectionProviderModuleDescriptor;

/**
 * Allows plugins to provide dynamic web-sections for a location and given context. This is useful when sections should
 * be inserted in the UI based on data.
 *
 * @since v3.0.2
 */
public interface WebSectionProvider {

    /**
     * Called after construction, but before {@link #getSections(Map)}
     *
     * @param moduleDescriptor for this instance of the {@link WebSectionProvider}
     * @since 7.2.0
     */
    default void init(@Nonnull WebSectionProviderModuleDescriptor<WebSectionProvider> moduleDescriptor) {
        // do nothing
    }

    /**
     * Returns a list of web-sections for a given context. May be null.
     *
     * @return a list of web-sections for a given context. May be null.
     */
    Iterable<WebSection> getSections(final Map<String, Object> context);
}
