# For everyone

## 1. Moving types (classes and interfaces) around

We introduced replacements for existing API. This has the following advantages;

- What is API is better defined, you should be able to get everything you need from the API maven module
- We were able to remove third-party types from the API decoupling from other dependencies such as Guava and Dom4j,
  which means:
    - webfragments is no longer a blocker in upgrading them
    - those dependencies don't need to be used to be able to use webfragments

Here is the list of affected classes and what to migrate to:

| Old fully-qualified type name                                         | New fully-qualified type name                                             |
|-----------------------------------------------------------------------|---------------------------------------------------------------------------|
| com.atlassian.plugin.web.api.model.AbstractWebFragment                | com.atlassian.plugin.web.api.WebFragment                                  |
| com.atlassian.plugin.web.api.model.WebItemImpl                        | com.atlassian.plugin.web.api.WebItem                                      |
| com.atlassian.plugin.web.api.model.WebSectionImpl                     | com.atlassian.plugin.web.api.WebSection                                   |
| com.atlassian.plugin.web.baseconditions.BaseCondition                 | com.atlassian.plugin.web.api.baseconditions.BaseCondition                 |
| com.atlassian.plugin.web.baseconditions.CompositeCondition            | com.atlassian.plugin.web.api.baseconditions.CompositeCondition            |
| com.atlassian.plugin.web.conditions.ConditionLoadingException         | com.atlassian.plugin.web.api.conditions.ConditionLoadingException         |
| com.atlassian.plugin.web.descriptors.ConditionalDescriptor            | com.atlassian.plugin.web.api.descriptors.ConditionalDescriptor            |
| com.atlassian.plugin.web.descriptors.ContextAware                     | com.atlassian.plugin.web.api.descriptors.ContextAware                     |
| com.atlassian.plugin.web.descriptors.WebFragmentModuleDescriptor      | com.atlassian.plugin.web.api.descriptors.WebFragmentModuleDescriptor      |
| com.atlassian.plugin.web.descriptors.WebItemModuleDescriptor          | com.atlassian.plugin.web.api.descriptors.WebItemModuleDescriptor          |
| com.atlassian.plugin.web.descriptors.WebPanelModuleDescriptor         | com.atlassian.plugin.web.api.descriptors.WebPanelModuleDescriptor         |
| com.atlassian.plugin.web.descriptors.WebSectionModuleDescriptor       | com.atlassian.plugin.web.api.descriptors.WebSectionModuleDescriptor       |
| com.atlassian.plugin.web.descriptors.WeightedDescriptor               | com.atlassian.plugin.web.api.descriptors.WeightedDescriptor               |
| com.atlassian.plugin.web.model.WebLink                                | com.atlassian.plugin.web.api.WebItem                                      |
| com.atlassian.plugin.web.model.WebPanel                               | com.atlassian.plugin.web.api.model.WebPanel                               |
| com.atlassian.plugin.web.descriptors.WebPanelRendererModuleDescriptor | com.atlassian.plugin.web.api.descriptors.WebPanelRendererModuleDescriptor |
| com.atlassian.plugin.web.renderer.RendererException                   | com.atlassian.plugin.web.api.renderer.RendererException                   |
| com.atlassian.plugin.web.renderer.WebPanelRenderer                    | com.atlassian.plugin.web.api.renderer.WebPanelRenderer                    |

## 2. Inaccessible implementation

Previously much of the implementation of web-fragments was accessible via OSGi and this has become hidden for the same
reasons in the previous step. We know there are some usages though, so here are the migration steps for what we've
detected being used;

### Element parsers - AbstractConditionElementParser, ConditionElementParser, ContextProviderElementParser, WeightElementParser

Instead of parsing the XML yourself, let webfragments do the work by using the `DynamicWebInterfaceManager` service

### Composite conditions - AbstractCompositeCondition, AndCompositeCondition, InvertedCondition, OrCompositeCondition

[Defining composite conditions in plugin descriptor XML](https://developer.atlassian.com/server/framework/atlassian-sdk/web-item-plugin-module/#condition-and-conditions-elements)
should be enough, if you dynamically register modules these are relatively easy to re-implement yourself.

### Definitive conditions - AlwaysDisplayCondition, NeverDisplayCondition

These aren't useful for anything outside of testing, if something is always displayed it shouldn't have a condition on
it, and if something is never displayed it shouldn't be defined. If you need these for testing, they are easy to
re-implement yourself

### Module descriptor implementations

The list of them is; AbstractWebFragmentModuleDescriptor, DefaultAbstractWebFragmentModuleDescriptor,
DefaultWebItemModuleDescriptor, DefaultWebPanelModuleDescriptor, DefaultWebSectionModuleDescriptor,
WebItemProviderModuleDescriptor, WebPanelRendererModuleDescriptor, and WebSectionProviderModuleDescriptor.

There is an interface that can be used instead for each of them under `com.atlassian.plugin.web.descriptors`
or `com.atlassian.plugin.web.provider`

Please refer to [WebPanelWiredTest](https://bitbucket.org/atlassian/atlassian-plugins-webfragment/src/8d833fcebb1f1b02700b6f8330fb024d17c46504/refapp-vendor-tests/src/test/java/it/com/vendor/test/wired/WebPanelWiredTest.java#WebPanelWiredTest.java) to see examples of how to use pocketknife to load modules dynamically

### DefaultWebInterfaceManager

This is the internal implementation we use, everything you need should be available on the
interface `DynamicWebInterfaceManager`

### NoOpContextProvider

Has no value outside of testing, it has no value being in production code where it'll cause confusion. If you really
need it, it's very simple to re-implement.

## 3. Removed API

### WeightedComparator

Using weights is still API, but the code that sorts them won't be to allow us to change how that's done. If you have any
tests that need updating, then it's possible to create your own comparator like
so: `Comparator.comparingInt(WebFragment::getWeight)`

### Implementations of fragments - AbstractWebFragment, WebFragmentBuilder, WebSectionImpl, WebItemImpl

It is unlikely this needs to be used by anyone developing a plugin, and it's relatively simple to do without them. These
were removed, so we can modify the internals of how we build webfragments in production code without breaking API.
Defining web [sections](https://developer.atlassian.com/server/framework/atlassian-sdk/web-section-plugin-module/)
and [items](https://developer.atlassian.com/server/framework/atlassian-sdk/web-item-plugin-module/) in plugin descriptor
XML should be enough, if you need to dynamically register them nothing stops you from implementing the interfaces
instead.

### WebPanelRenderer#renderFragment(String, Plugin, Map<String, Object>)

This method only causes confusion because it's never called, you will have already
implemented `WebPanelRenderer#renderFragment(Writer, String, Plugin, Map<String, Object>)` which makes the migration
easy, just delete the implementation of the old method. Within your implementation of the method, it's best to lean on
the writer as much as possible rather than creating a large string and then writing it at the end for hardware-resource
efficiency.

### Removal of `WebParam`

The parent interface of all the web fragment module descriptors is `ModuleDescriptor` which has the `#getParams` method. Similarly `WebPanel`, `WebSection`, and `WebItem` all extend from `WebFragment` which has `#getParams()`.

### Removal of `WebIcon`, `WebLink`, and `WebLabel`

Don't worry, these can still be set in the plugin descriptor (`atlassian-plugin.xml`) file. The Java types are no longer API. Instead, use the replacement methods on the interfaces instead, i.e. `WebFragment#getLabel`, `WebItem#getUrl`.

There was no recorded usage of `WebIcon`'s methods, nor getting the tooltip of a `WebItem` so there is no replacement. If you need this please create a ticket on https://ecosystem.atlassian.net/browse/PLUGFRAG with the use-case described. In the mean-time there's nothing stopping the XML being read directly.

# For product and platform teams

## 1. Moved types

To minimise the impact on everyone, we've kept specifically `com.atlassian.plugin.web` public, but to achieve this we've
moved some implementation classes. You will have to update wiring within the system bundle.

| Old fully-qualified type name                       | New fully-qualified type name                            |
|-----------------------------------------------------|----------------------------------------------------------|
| com.atlassian.plugin.web.DefaultWebInterfaceManager | com.atlassian.plugin.web.impl.DefaultWebInterfaceManager |
| com.atlassian.plugin.web.NoOpContextProvider        | com.atlassian.plugin.web.impl.NoOpContextProvider        |
| com.atlassian.plugin.web.WebInterfaceManager        | com.atlassian.plugin.web.impl.DefaultWebInterfaceManager |

## 2. DMZ update

The products and platform team should ensure these access-levels match these packages.

| Package                                     | Access level                    |
|---------------------------------------------|---------------------------------|
| com.atlassian.plugin.web                    | Public                          |
| com.atlassian.plugin.web.api                | Public                          |
| com.atlassian.plugin.web.api.baseconditions | Public                          |
| com.atlassian.plugin.web.api.descriptors    | Public                          |
| com.atlassian.plugin.web.api.model          | Public                          |
| com.atlassian.plugin.web.api.provider       | Public                          | 
| com.atlassian.plugin.web.api.renderer       | Public                          | 
| com.atlassian.plugin.web.baseconditions     | Ideally private, but accept DMZ | 
| com.atlassian.plugin.web.conditions         | Ideally private, but accept DMZ |
| com.atlassian.plugin.web.descriptors        | Ideally private, but accept DMZ |
| com.atlassian.plugin.web.impl               | Ideally private, but accept DMZ |
| com.atlassian.plugin.web.model              | Ideally private, but accept DMZ |
| com.atlassian.plugin.web.renderer           | Ideally private, but accept DMZ |

### Module descriptor implementations

If you struggle with the `WebInterfaceManager` methods which return `WebSectionModuleDescriptor`, `WebItemModuleDescriptor` or `WebPanelModuleDescriptor` and you have a type error because you import these classes from the `com.atlassian.plugin.web.descriptors` package and `WebInterfaceManager` returns the classes from the `com.atlassian.plugin.web.api.descriptors` package, then _cast_ these classes to the old one from the `com.atlassian.plugin.web.descriptors` package.

Before:
```
import com.atlassian.plugin.web.descriptors.WebItemModuleDescriptor;

for (final WebItemModuleDescriptor descriptor : webInterfaceManager.getDisplayableItems(location, contextMap)) { // ERROR! type mismatch
  final WebLink link = descriptor.getLink(); // ERROR! method does not exist
  ...
}
```

After:
```
for (final WebItemModuleDescriptor descriptorApi : webInterfaceManager.getDisplayableItems(location, contextMap)) {
  com.atlassian.plugin.web.descriptors.WebItemModuleDescriptor descriptor = (com.atlassian.plugin.web.descriptors.WebItemModuleDescriptor) descriptorApi;
  final WebLink link = descriptor.getLink();
  ...
}
```

## 3. Removal of private signatures

### Instantiating `DefaultWebInterfaceManager`

Please use the constructor with the most arguments. The one with `EventPublisher` has been removed, and the no-args one will be removed in 7.0.0

### Removal of `WebLabel#getNoKeyValue`

This is unused, just delete it from your decorator interfaces

## 4. Updating of your own API

Double check none of the implementation classes are referenced in your own API. With the DMZ in effect, vendors won't be able to access the types anyway, but failure to remove references to implementation classes will cause the rest of your service API to break. 

## 5. `WebIcon`, `WebLink`, and `WebLabel`

These are still used for rendering products so these can still be accessed by using the implementation types instead of the interfaces.
