package com.example.vendor.test.servlet;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.plugin.web.api.DynamicWebInterfaceManager;
import com.atlassian.plugin.web.api.model.WebPanel;
import com.atlassian.soy.renderer.SoyTemplateRenderer;

public class SoyServlet extends HttpServlet {
    private static final String TEMPLATE_HTML_CONTENT_TYPE = "text/html;charset=UTF-8";

    private final SoyTemplateRenderer soyTemplateRenderer;
    private final DynamicWebInterfaceManager webInterfaceManager;

    @Inject
    public SoyServlet(SoyTemplateRenderer soyTemplateRenderer, DynamicWebInterfaceManager webInterfaceManager) {
        this.soyTemplateRenderer = soyTemplateRenderer;
        this.webInterfaceManager = webInterfaceManager;
    }

    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) {
        List<? extends WebPanel> panels =
                webInterfaceManager.getDisplayableWebPanels("com.vendor.test.page", Collections.emptyMap());

        List<String> panelsHtml = panels.stream()
                .map(panel -> panel.getHtml(Collections.emptyMap()))
                .toList();

        Map<String, Object> params = new HashMap<>();
        params.put("panels", panelsHtml);

        String content = soyTemplateRenderer.render(
                "com.example.vendor.atlassian-plugins-webfragment-sample-vendor-plugin:soy-templates",
                "com.example.vendor.test.page.index",
                params);
        response.setContentType(TEMPLATE_HTML_CONTENT_TYPE);
        try {
            response.getWriter().write(content);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
