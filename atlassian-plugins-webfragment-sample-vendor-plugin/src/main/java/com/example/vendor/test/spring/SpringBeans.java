package com.example.vendor.test.spring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.atlassian.plugin.web.api.DynamicWebInterfaceManager;
import com.atlassian.soy.renderer.SoyTemplateRenderer;

import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.importOsgiService;

@Configuration
public class SpringBeans {

    // ----------------- OSGI SERVICE IMPORTS -----------------

    @Bean
    public SoyTemplateRenderer soyTemplateRenderer() {
        return importOsgiService(SoyTemplateRenderer.class);
    }

    @Bean
    public DynamicWebInterfaceManager dynamicWebInterfaceManager() {
        return importOsgiService(DynamicWebInterfaceManager.class);
    }
}
