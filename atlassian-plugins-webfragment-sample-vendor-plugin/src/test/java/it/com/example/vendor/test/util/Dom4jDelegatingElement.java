package it.com.example.vendor.test.util;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.dom4j.Node;

import com.atlassian.plugin.module.Element;

public class Dom4jDelegatingElement implements Element {
    private final org.dom4j.Element dom4jElement;

    public Dom4jDelegatingElement(org.dom4j.Element dom4jElement) {
        this.dom4jElement = dom4jElement;
    }

    public String attributeValue(String key) {
        return this.dom4jElement == null ? null : this.dom4jElement.attributeValue(key);
    }

    public String attributeValue(String key, String defaultValue) {
        String attributeValue = this.attributeValue(key);
        return attributeValue == null ? defaultValue : attributeValue;
    }

    public Element element(String element) {
        if (this.dom4jElement == null) {
            return null;
        } else {
            return this.dom4jElement.element(element) == null
                    ? null
                    : new Dom4jDelegatingElement(this.dom4jElement.element(element));
        }
    }

    public List<Element> elements(String name) {
        return this.dom4jElement == null
                ? Collections.emptyList()
                : (List) this.dom4jElement.elements(name).stream()
                        .map(Dom4jDelegatingElement::new)
                        .collect(Collectors.toList());
    }

    public String getTextTrim() {
        return this.dom4jElement == null ? "" : this.dom4jElement.getTextTrim();
    }

    public String elementTextTrim(String name) {
        return this.dom4jElement == null ? null : this.dom4jElement.elementTextTrim(name);
    }

    public String getText() {
        return this.dom4jElement == null ? "" : this.dom4jElement.getText();
    }

    public String getName() {
        return this.dom4jElement == null ? null : this.dom4jElement.getName();
    }

    public List<Element> elements() {
        return this.dom4jElement == null
                ? Collections.emptyList()
                : (List) this.dom4jElement.elements().stream()
                        .map(Dom4jDelegatingElement::new)
                        .collect(Collectors.toList());
    }

    public List<String> attributeNames() {
        return this.dom4jElement == null
                ? Collections.emptyList()
                : (List) this.dom4jElement.attributes().stream()
                        .map(Node::getName)
                        .collect(Collectors.toList());
    }

    public org.dom4j.Element getDelegate() {
        return this.dom4jElement;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        } else if (o != null && this.getClass() == o.getClass()) {
            Dom4jDelegatingElement that = (Dom4jDelegatingElement) o;
            return Objects.equals(this.dom4jElement, that.dom4jElement);
        } else {
            return false;
        }
    }

    public int hashCode() {
        return Objects.hash(new Object[] {this.dom4jElement});
    }
}
