package it.com.example.vendor.test.wired;

import java.io.InputStream;

import org.dom4j.dom.DOMElement;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;

import it.com.example.vendor.test.util.Dom4jDelegatingElement;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.module.Element;
import com.atlassian.plugins.osgi.test.AtlassianPluginsTestRunner;
import com.atlassian.pocketknife.api.lifecycle.modules.DynamicModuleDescriptorFactory;
import com.atlassian.pocketknife.api.lifecycle.modules.LoaderConfiguration;
import com.atlassian.pocketknife.api.lifecycle.modules.ModuleRegistrationHandle;
import com.atlassian.pocketknife.api.lifecycle.modules.ResourceLoader;

import static it.com.example.vendor.test.util.WebDriverTestUtil.navigate;
import static it.com.example.vendor.test.util.WebDriverTestUtil.waitForElement;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@RunWith(AtlassianPluginsTestRunner.class)
public class WebPanelWiredTest {
    private static final String TEST_PLUGIN_KEY =
            "com.example.vendor.atlassian-plugins-webfragment-sample-vendor-plugin-tests";
    private static final String TEST_SERVLET_PATH = "/plugins/servlet/web-panels";

    private final PluginAccessor pluginAccessor;
    private final DynamicModuleDescriptorFactory dynamicModuleDescriptorFactory;

    public WebPanelWiredTest(
            PluginAccessor pluginAccessor, DynamicModuleDescriptorFactory dynamicModuleDescriptorFactory) {
        this.pluginAccessor = pluginAccessor;
        this.dynamicModuleDescriptorFactory = dynamicModuleDescriptorFactory;
    }

    @Test
    public void createDynamicWebPanelFromXml() {
        ModuleRegistrationHandle registrationHandle = addDynamicModuleFromXml("/dynamic/define-web-panel.xml");

        Plugin plugin = pluginAccessor.getPlugin(TEST_PLUGIN_KEY);
        boolean hasDynamicModule = plugin.getModuleDescriptors().stream()
                .anyMatch(moduleDescriptor -> moduleDescriptor.getKey().equals("dynamic-test-panel-xml"));
        assertThat(hasDynamicModule).isTrue();

        navigate(TEST_SERVLET_PATH);
        boolean panelWasLoaded = waitForElement(By.id("dynamic-xml-content"));
        assertThat(panelWasLoaded).isTrue();

        // clean up
        registrationHandle.unregister();
    }

    @Test
    public void createDynamicWebPanelFromElement() {
        Element webPanelElement = createWebPanelElement();
        ModuleRegistrationHandle registrationHandle = addDynamicModuleFromElement(webPanelElement);

        Plugin plugin = pluginAccessor.getPlugin(TEST_PLUGIN_KEY);
        boolean hasDynamicModule = plugin.getModuleDescriptors().stream()
                .anyMatch(moduleDescriptor -> moduleDescriptor.getKey().equals("dynamic-test-panel"));
        assertThat(hasDynamicModule).isTrue();

        navigate(TEST_SERVLET_PATH);
        boolean panelWasLoaded = waitForElement(By.id("dynamic-element-content"));
        assertThat(panelWasLoaded).isTrue();

        // clean up
        registrationHandle.unregister();
    }

    private ModuleRegistrationHandle addDynamicModuleFromXml(String pluginXml) {
        Plugin plugin = pluginAccessor.getPlugin(TEST_PLUGIN_KEY);
        LoaderConfiguration loaderConfiguration = new LoaderConfiguration(plugin);
        loaderConfiguration.addPathsToAuxAtlassianPluginXMLs(pluginXml);
        loaderConfiguration.setResourceLoader(new PluginResourceLoader());

        return dynamicModuleDescriptorFactory.loadModules(loaderConfiguration);
    }

    private ModuleRegistrationHandle addDynamicModuleFromElement(Element element) {
        Plugin plugin = pluginAccessor.getPlugin(TEST_PLUGIN_KEY);

        return dynamicModuleDescriptorFactory.loadModules(plugin, element);
    }

    private Element createWebPanelElement() {
        org.dom4j.Element resourceElement = new DOMElement("resource");
        resourceElement.addAttribute("name", "view");
        resourceElement.addAttribute("type", "static");
        resourceElement.addText("<div id=\"dynamic-element-content\">Hello World from dynamic element!</div>");

        org.dom4j.Element rootElement = new DOMElement("web-panel");
        rootElement.addAttribute("key", "dynamic-test-panel");
        rootElement.addAttribute("location", "com.vendor.test.page");
        rootElement.add(resourceElement);

        return new Dom4jDelegatingElement(rootElement);
    }

    private class PluginResourceLoader implements ResourceLoader {
        @Override
        public InputStream getResourceAsStream(String s) {
            return WebPanelWiredTest.this.getClass().getClassLoader().getResourceAsStream(s);
        }
    }
}
