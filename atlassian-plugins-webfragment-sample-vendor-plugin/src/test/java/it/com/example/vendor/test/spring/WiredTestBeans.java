package it.com.example.vendor.test.spring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.osgi.framework.BundleContext;

import it.com.example.vendor.test.wired.WebPanelWiredTest;

import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugins.osgi.javaconfig.configs.beans.ModuleFactoryBean;
import com.atlassian.plugins.osgi.javaconfig.configs.beans.PluginAccessorBean;
import com.atlassian.pocketknife.api.lifecycle.modules.DynamicModuleDescriptorFactory;
import com.atlassian.pocketknife.internal.lifecycle.modules.CombinedModuleDescriptorFactoryProvider;
import com.atlassian.pocketknife.internal.lifecycle.modules.DynamicModuleDescriptorFactoryImpl;
import com.atlassian.pocketknife.internal.lifecycle.modules.DynamicModuleRegistration;

@Import({PluginAccessorBean.class, ModuleFactoryBean.class})
@Configuration
public class WiredTestBeans {
    @Bean
    public WebPanelWiredTest webPanelWiredTest(
            PluginAccessor pluginAccessor, DynamicModuleDescriptorFactory dynamicModuleDescriptorFactory) {
        return new WebPanelWiredTest(pluginAccessor, dynamicModuleDescriptorFactory);
    }

    @Bean
    public DynamicModuleRegistration dynamicModuleRegistration(BundleContext bundleContext) {
        return new DynamicModuleRegistration(bundleContext);
    }

    @Bean
    public CombinedModuleDescriptorFactoryProvider combinedModuleDescriptorFactoryProvider() {
        return new CombinedModuleDescriptorFactoryProvider();
    }

    @Bean
    public DynamicModuleDescriptorFactory dynamicModuleDescriptorFactory(
            DynamicModuleRegistration dynamicModuleRegistration,
            CombinedModuleDescriptorFactoryProvider combinedModuleDescriptorFactoryProvider) {
        return new DynamicModuleDescriptorFactoryImpl(
                dynamicModuleRegistration, combinedModuleDescriptorFactoryProvider);
    }
}
