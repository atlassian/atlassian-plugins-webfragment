package it.com.example.vendor.test.util;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.atlassian.pageobjects.elements.query.AbstractTimedQuery;
import com.atlassian.pageobjects.elements.query.ExpirationHandler;
import com.atlassian.pageobjects.elements.timeout.DefaultTimeouts;
import com.atlassian.pageobjects.elements.timeout.TimeoutType;
import com.atlassian.pageobjects.elements.timeout.Timeouts;
import com.atlassian.webdriver.refapp.RefappTestedProduct;

import static java.util.Objects.requireNonNull;

import static com.atlassian.pageobjects.TestedProductFactory.create;

public final class WebDriverTestUtil {

    public static final RefappTestedProduct REFAPP = create(RefappTestedProduct.class);

    private WebDriverTestUtil() {}

    public static void navigate(@Nonnull final String servlet) {
        requireNonNull(servlet, "The servlet path is mandatory.");
        REFAPP.getTester().getDriver().navigate().to(REFAPP.getProductInstance().getBaseUrl() + servlet);
    }

    @Nullable
    public static WebElement findElement(@Nonnull final By selector) {
        requireNonNull(selector, "The selector is mandatory.");
        return REFAPP.getTester().getDriver().findElement(selector);
    }

    public static boolean waitForElement(By selector) {
        final AbstractTimedQuery<Boolean> getElementLoaded =
                new AbstractTimedQuery<>(
                        new DefaultTimeouts().timeoutFor(TimeoutType.PAGE_LOAD),
                        Timeouts.DEFAULT_INTERVAL,
                        ExpirationHandler.RETURN_CURRENT) {
                    @Override
                    protected boolean shouldReturn(Boolean currentEval) {
                        return currentEval;
                    }

                    @Override
                    protected Boolean currentValue() {
                        try {
                            WebElement el = findElement(selector);
                            return el != null; // found it.
                        } catch (RuntimeException e) {
                            // swallow NoSuchElementException.
                            return false;
                        }
                    }
                };

        return getElementLoaded.byDefaultTimeout();
    }
}
