# Host application upgrade notes

Version 5.2.0 includes more analytics to track how many slow webfragment conditions there are

## Analytics

No effort is required to turn the analytics on, they are on by default.

You will need to update analytics whitelist repo for your products with the
event: `web.fragment.condition.evaluate.time`
and whitelist the attribute `fragmentLocation`, then deploy, then upgrade the version in the product.

NOTE: This will mean whatever is passed in will be sent RAW to our analytics pipeline. Please make sure that this value
will not contain UGC. It should be a value that our products have defined.
