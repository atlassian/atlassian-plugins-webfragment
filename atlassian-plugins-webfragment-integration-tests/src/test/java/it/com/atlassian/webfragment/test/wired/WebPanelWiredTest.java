package it.com.atlassian.webfragment.test.wired;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.atlassian.plugin.web.api.DynamicWebInterfaceManager;
import com.atlassian.plugin.web.descriptors.DefaultWebPanelModuleDescriptor;
import com.atlassian.plugin.web.descriptors.WebFragmentModuleDescriptor;
import com.atlassian.plugin.web.descriptors.WebPanelModuleDescriptor;
import com.atlassian.plugins.osgi.test.AtlassianPluginsTestRunner;

import static java.util.Collections.emptyMap;
import static java.util.stream.StreamSupport.stream;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(AtlassianPluginsTestRunner.class)
public class WebPanelWiredTest {

    private final DynamicWebInterfaceManager dynamicWebInterfaceManager;

    public WebPanelWiredTest(DynamicWebInterfaceManager dynamicWebInterfaceManager) {
        this.dynamicWebInterfaceManager = dynamicWebInterfaceManager;
    }

    @Test
    public void smokeTest_givenAWebPanelIsDefined_whenGettingWebPanelsForALocation_thenTheWebPanelShouldBeProvided() {
        var webPanels = dynamicWebInterfaceManager.getWebPanels("atl.refapp.index");
        assertThat(webPanels)
                .anyMatch(webPanel -> webPanel.getHtml(emptyMap()).contains("Static Web-Panel for testing"));
    }

    @Test
    public void
            smokeTest_givenAWebPanelIsDefinedWithoutACondidtion_whenGettingWebPanelsForALocation_thenTheWebPanelShouldBeDisplayable() {
        var webPanels = dynamicWebInterfaceManager.getDisplayableWebPanels("atl.refapp.index", emptyMap());
        assertThat(webPanels)
                .anyMatch(webPanel -> webPanel.getHtml(emptyMap()).contains("Static Web-Panel for testing"));
    }

    @Test
    public void testWebPanelIsCastable() {
        var webPanels = dynamicWebInterfaceManager.getDisplayableWebPanels("atl.refapp.index", emptyMap());
        var webPanel = stream(webPanels.spliterator(), false)
                .filter(candidate -> candidate.getHtml(emptyMap()).contains("Static Web-Panel for testing"))
                .findFirst()
                .get();

        assertThat(webPanel).isInstanceOf(com.atlassian.plugin.web.model.WebPanel.class);
    }

    @Test
    public void
            smokeTest_givenAWebPanelIsDefined_whenGettingWebPanelDescriptorsForALocation_thenTheWebPanelShouldBeProvided() {
        var webPanelDescriptors = dynamicWebInterfaceManager.getWebPanelDescriptors("atl.refapp.index");
        assertThat(webPanelDescriptors).anyMatch(webPanel -> webPanel.getKey().equals("web-panel-static"));
    }

    @Test
    public void
            smokeTest_givenAWebPanelIsDefinedWithoutACondidtion_whenGettingWebPanelDescriptorsForALocation_thenTheWebPanelShouldBeDisplayable() {
        var webPanelDescriptors =
                dynamicWebInterfaceManager.getDisplayableWebPanelDescriptors("atl.refapp.index", emptyMap());
        assertThat(webPanelDescriptors).anyMatch(webPanel -> webPanel.getKey().equals("web-panel-static"));
    }

    /**
     * Because external plugins can use this API and because our implementations are behind the DMZ (i.e. private), we
     * cannot expect vendors to use our implementation.
     */
    @Test
    public void
            givenAWebPanelIsDefined_whenGettingWebPanelDescriptorsForALocation_thenTheWebPanelShouldBeCastableToInternalTypes() {
        var webPanelDescriptors =
                dynamicWebInterfaceManager.getDisplayableWebPanelDescriptors("atl.refapp.index", emptyMap());
        var webPanelModuleDescriptor = webPanelDescriptors.stream()
                .filter(candidate -> candidate.getKey().equals("web-panel-static"))
                .findFirst()
                .get();

        assertThat(webPanelModuleDescriptor).isInstanceOf(WebFragmentModuleDescriptor.class);
        assertThat(webPanelModuleDescriptor).isInstanceOf(WebPanelModuleDescriptor.class);
        assertThat(webPanelModuleDescriptor).isInstanceOf(DefaultWebPanelModuleDescriptor.class);
    }
}
