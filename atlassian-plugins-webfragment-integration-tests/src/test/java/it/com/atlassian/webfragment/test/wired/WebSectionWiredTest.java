package it.com.atlassian.webfragment.test.wired;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.atlassian.plugin.web.api.DynamicWebInterfaceManager;
import com.atlassian.plugin.web.descriptors.DefaultWebSectionModuleDescriptor;
import com.atlassian.plugin.web.descriptors.WebFragmentModuleDescriptor;
import com.atlassian.plugin.web.descriptors.WebSectionModuleDescriptor;
import com.atlassian.plugins.osgi.test.AtlassianPluginsTestRunner;

import static java.util.Collections.emptyMap;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(AtlassianPluginsTestRunner.class)
public class WebSectionWiredTest {

    private final DynamicWebInterfaceManager dynamicWebInterfaceManager;

    public WebSectionWiredTest(DynamicWebInterfaceManager dynamicWebInterfaceManager) {
        this.dynamicWebInterfaceManager = dynamicWebInterfaceManager;
    }

    @Test
    public void
            smokeTest_givenAWebSectionIsDefined_whenGettingWebSectionsForALocation_thenTheWebSectionShouldBeProvided() {
        var webSections = dynamicWebInterfaceManager.getWebSections("system.admin", emptyMap());
        assertThat(webSections)
                .anyMatch(webSection -> webSection.getCompleteKey().endsWith(":web-section"));
    }

    @Test
    public void
            smokeTest_givenAWebSectionIsDefinedWithoutACondidtion_whenGettingWebSectionsForALocation_thenTheWebSectionShouldBeDisplayable() {
        var webSections = dynamicWebInterfaceManager.getDisplayableWebSections("system.admin", emptyMap());
        assertThat(webSections)
                .anyMatch(webSection -> webSection.getCompleteKey().endsWith(":web-section"));
    }

    @Test
    public void
            smokeTest_givenAWebSectionIsDefined_whenGettingWebSectionDescriptorsForALocation_thenTheWebSectionShouldBeProvided() {
        var webSections = dynamicWebInterfaceManager.getSections("system.admin");
        assertThat(webSections).anyMatch(webSection -> webSection.getKey().equals("web-section"));
    }

    @Test
    public void
            smokeTest_givenAWebSectionIsDefinedWithoutACondidtion_whenGettingWebSectionDescriptorsForALocation_thenTheWebSectionShouldBeDisplayable() {
        var webSections = dynamicWebInterfaceManager.getDisplayableSections("system.admin", emptyMap());
        assertThat(webSections).anyMatch(webSection -> webSection.getKey().equals("web-section"));
    }

    /**
     * Why? To support our own internal code that hasn't moved to the new interface yet
     */
    @Test
    public void
            givenAWebSectionIsDefined_whenGettingWebSectionDescriptorsForALocation_thenTheWebSectionShouldBeCastableToInternalTypes() {
        var webSections = dynamicWebInterfaceManager.getDisplayableSections("system.admin", emptyMap());
        var webSection = webSections.stream()
                .filter(candidate -> candidate.getKey().equals("web-section"))
                .findFirst()
                .get();

        assertThat(webSection).isInstanceOf(WebFragmentModuleDescriptor.class);
        assertThat(webSection).isInstanceOf(WebSectionModuleDescriptor.class);
        assertThat(webSection).isInstanceOf(DefaultWebSectionModuleDescriptor.class);
    }
}
