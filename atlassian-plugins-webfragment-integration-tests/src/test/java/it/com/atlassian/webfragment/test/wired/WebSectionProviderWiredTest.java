package it.com.atlassian.webfragment.test.wired;

import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.atlassian.plugin.web.api.DynamicWebInterfaceManager;
import com.atlassian.plugin.web.api.WebSection;
import com.atlassian.plugin.web.model.AbstractWebFragment;
import com.atlassian.plugin.web.model.WebSectionImpl;
import com.atlassian.plugins.osgi.test.AtlassianPluginsTestRunner;

import static java.util.Collections.emptyMap;
import static java.util.stream.StreamSupport.stream;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(AtlassianPluginsTestRunner.class)
public class WebSectionProviderWiredTest {

    private final DynamicWebInterfaceManager dynamicWebInterfaceManager;

    public WebSectionProviderWiredTest(DynamicWebInterfaceManager dynamicWebInterfaceManager) {
        this.dynamicWebInterfaceManager = dynamicWebInterfaceManager;
    }

    @Test
    public void
            smokeTest_givenAWebSectionIsProvided_whenGettingWebSectionsForALocation_thenTheWebSectionShouldBeProvided() {
        var webSections = dynamicWebInterfaceManager.getWebSections("system.admin", emptyMap());
        assertThat(webSections)
                .anyMatch(webSection -> webSection.getCompleteKey().endsWith(":web-section-provided"));
    }

    @Test
    public void
            smokeTest_givenAWebSectionIsProvidedWithoutACondidtion_whenGettingWebSectionsForALocation_thenTheWebSectionShouldBeDisplayable() {
        var webSections = dynamicWebInterfaceManager.getDisplayableWebSections("system.admin", emptyMap());
        assertThat(webSections)
                .anyMatch(webSection -> webSection.getCompleteKey().endsWith(":web-section-provided"));
    }

    /**
     * Because external plugins can use this API and because our implementations are behind the DMZ (i.e. private), we
     * cannot expect vendors to use our implementation.
     */
    @Test
    public void
            givenAWebSectionIsProvided_whenGettingWebSectionsForALocation_thenTheWebSectionHasNoGuaranteeOfUsingOurInternalImplementation() {
        var webSections = dynamicWebInterfaceManager.getDisplayableWebSections("system.admin", emptyMap());
        var webSection = stream(webSections.spliterator(), false)
                .filter(candidate -> candidate.getCompleteKey().endsWith(":web-section-provided"))
                .findFirst()
                .get();

        assertThat(webSection).isNotInstanceOfAny(AbstractWebFragment.class, WebSectionImpl.class);
    }

    @Test
    public void
            givenTwoWebItemProvidersWithTheSameImplementationButDifferentParams_whenFetchingTheirWebItems_thenTheirParamsShouldNotBeConfused() {
        var webSections = stream(
                        dynamicWebInterfaceManager
                                .getWebSections(
                                        "web-section-provider-with-reused-implementation-but-different-params",
                                        emptyMap())
                                .spliterator(),
                        false)
                .toList();

        assertThat(webSections).hasSize(2);
        assertThat(webSections)
                .first()
                .extracting(WebSection::getParams)
                .extracting(Map::entrySet)
                .isEqualTo(Map.of("web-section-provider-1", "web-section-provider-1")
                        .entrySet());
        assertThat(webSections)
                .last()
                .extracting(WebSection::getParams)
                .extracting(Map::entrySet)
                .isEqualTo(Map.of("web-section-provider-2", "web-section-provider-2")
                        .entrySet());
    }
}
