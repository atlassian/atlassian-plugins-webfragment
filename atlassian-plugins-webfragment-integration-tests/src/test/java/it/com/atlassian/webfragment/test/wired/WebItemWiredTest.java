package it.com.atlassian.webfragment.test.wired;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.atlassian.plugin.web.api.DynamicWebInterfaceManager;
import com.atlassian.plugin.web.descriptors.DefaultWebItemModuleDescriptor;
import com.atlassian.plugin.web.descriptors.WebFragmentModuleDescriptor;
import com.atlassian.plugin.web.descriptors.WebItemModuleDescriptor;
import com.atlassian.plugins.osgi.test.AtlassianPluginsTestRunner;

import static java.util.Collections.emptyMap;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(AtlassianPluginsTestRunner.class)
public class WebItemWiredTest {

    private final DynamicWebInterfaceManager dynamicWebInterfaceManager;

    public WebItemWiredTest(DynamicWebInterfaceManager dynamicWebInterfaceManager) {

        this.dynamicWebInterfaceManager = dynamicWebInterfaceManager;
    }

    @Test
    public void smokeTest_givenAWebItemIsDefined_whenGettingWebItemsForALocation_thenTheWebItemShouldBeProvided() {
        var webItems = dynamicWebInterfaceManager.getWebItems("system.admin/general", emptyMap());
        assertThat(webItems).anyMatch(webItem -> webItem.getCompleteKey().endsWith(":web-item"));
    }

    @Test
    public void
            smokeTest_givenAWebItemIsDefinedWithoutACondidtion_whenGettingWebItemsForALocation_thenTheWebItemShouldBeDisplayable() {
        var webItems = dynamicWebInterfaceManager.getDisplayableWebItems("system.admin/general", emptyMap());
        assertThat(webItems).anyMatch(webItem -> webItem.getCompleteKey().endsWith(":web-item"));
    }

    @Test
    public void
            smokeTest_givenAWebItemIsDefined_whenGettingWebItemDescriptorsForALocation_thenTheWebItemShouldBeProvided() {
        var webItems = dynamicWebInterfaceManager.getItems("system.admin/general");
        assertThat(webItems).anyMatch(webItem -> webItem.getKey().equals("web-item"));
    }

    @Test
    public void
            smokeTest_givenAWebItemIsDefinedWithoutACondidtion_whenGettingWebItemDescriptorsForALocation_thenTheWebItemShouldBeDisplayable() {
        var webItems = dynamicWebInterfaceManager.getDisplayableItems("system.admin/general", emptyMap());
        assertThat(webItems).anyMatch(webItem -> webItem.getKey().equals("web-item"));
    }

    /**
     * Why? 1. These have more API than what is present publicly 2. To provide a backward compatibility layer with code
     * we haven't updated to use the new API yet
     */
    @Test
    public void
            givenAWebItemIsDefined_whenGettingWebItemDescriptorsForALocation_thenTheWebItemShouldBeCastableToInternalTypes() {
        var webItems = dynamicWebInterfaceManager.getDisplayableItems("system.admin/general", emptyMap());
        var webItem = webItems.stream()
                .filter(candidate -> candidate.getKey().equals("web-item"))
                .findFirst()
                .get();

        assertThat(webItem).isInstanceOf(WebFragmentModuleDescriptor.class);
        assertThat(webItem).isInstanceOf(WebItemModuleDescriptor.class);
        assertThat(webItem).isInstanceOf(DefaultWebItemModuleDescriptor.class);
    }
}
