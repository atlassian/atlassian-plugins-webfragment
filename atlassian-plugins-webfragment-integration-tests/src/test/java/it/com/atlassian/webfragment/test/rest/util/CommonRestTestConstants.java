package it.com.atlassian.webfragment.test.rest.util;

public class CommonRestTestConstants {

    public static String getRefappRestBaseUrl() {
        return "http://127.0.0.1:5990/refapp/rest";
    }

    public static String getAdminUser() {
        return "admin";
    }

    public static String getAdminPassword() {
        return "admin";
    }

    public static String getNoisyNeighbourUrl(String path) {
        return getRefappRestBaseUrl() + "/noisyneighbour/latest" + path;
    }
}
