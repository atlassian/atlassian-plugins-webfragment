package it.com.atlassian.webfragment.test.wired;

import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.atlassian.plugin.web.api.DynamicWebInterfaceManager;
import com.atlassian.plugin.web.api.WebItem;
import com.atlassian.plugin.web.model.AbstractWebFragment;
import com.atlassian.plugin.web.model.WebItemImpl;
import com.atlassian.plugins.osgi.test.AtlassianPluginsTestRunner;

import static java.util.Collections.emptyMap;
import static java.util.stream.StreamSupport.stream;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * TODO move those tests to test directory so they run on CI
 * https://hello.jira.atlassian.cloud/browse/DCA11Y-1112
 */
@RunWith(AtlassianPluginsTestRunner.class)
public class WebItemProviderWiredTest {

    private final DynamicWebInterfaceManager dynamicWebInterfaceManager;

    public WebItemProviderWiredTest(DynamicWebInterfaceManager dynamicWebInterfaceManager) {

        this.dynamicWebInterfaceManager = dynamicWebInterfaceManager;
    }

    @Test
    public void smokeTest_givenAWebItemIsProvided_whenGettingWebItemsForALocation_thenTheWebItemShouldBeProvided() {
        var webItems = dynamicWebInterfaceManager.getWebItems("system.admin/general", emptyMap());
        assertThat(webItems).anyMatch(webItem -> webItem.getCompleteKey().endsWith(":web-item-provided"));
    }

    @Test
    public void
            smokeTest_givenAWebItemIsProvidedWithoutACondidtion_whenGettingWebItemsForALocation_thenTheWebItemShouldBeDisplayable() {
        var webItems = dynamicWebInterfaceManager.getDisplayableWebItems("system.admin/general", emptyMap());
        assertThat(webItems).anyMatch(webItem -> webItem.getCompleteKey().endsWith(":web-item-provided"));
    }

    /**
     * Because external plugins can use this API and because our implementations are behind the DMZ (i.e. private), we
     * cannot expect vendors to use our implementation.
     */
    @Test
    public void
            givenAWebItemIsProvided_whenGettingWebItemsForALocation_thenTheWebItemHasNoGuaranteeOfUsingOurInternalImplementation() {
        var webItems = dynamicWebInterfaceManager.getDisplayableWebItems("system.admin/general", emptyMap());
        var webItem = stream(webItems.spliterator(), false)
                .filter(candidate -> candidate.getCompleteKey().endsWith(":web-item-provided"))
                .findFirst()
                .get();

        assertThat(webItem).isNotInstanceOfAny(AbstractWebFragment.class, WebItemImpl.class);
    }

    @Test
    public void
            givenTwoWebItemProvidersWithTheSameImplementationButDifferentParams_whenFetchingTheirWebItems_thenTheirParamsShouldNotBeConfused() {
        var webItems = stream(
                        dynamicWebInterfaceManager
                                .getWebItems(
                                        "web-item-provider-with-reused-implementation-but-different-params", emptyMap())
                                .spliterator(),
                        false)
                .toList();

        assertThat(webItems).hasSize(2);
        assertThat(webItems)
                .first()
                .extracting(WebItem::getParams)
                .extracting(Map::entrySet)
                .isEqualTo(Map.of("web-item-provider-1", "web-item-provider-1").entrySet());
        assertThat(webItems)
                .last()
                .extracting(WebItem::getParams)
                .extracting(Map::entrySet)
                .isEqualTo(Map.of("web-item-provider-2", "web-item-provider-2").entrySet());
    }
}
