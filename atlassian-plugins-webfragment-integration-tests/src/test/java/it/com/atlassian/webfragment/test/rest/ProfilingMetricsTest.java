package it.com.atlassian.webfragment.test.rest;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.Nonnull;

import org.junit.Test;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import it.com.atlassian.webfragment.test.rest.util.CommonRestTestConstants;

import static io.restassured.http.ContentType.JSON;
import static javax.ws.rs.core.Response.Status.OK;
import static org.assertj.core.api.Assertions.assertThat;

public class ProfilingMetricsTest {

    private static final String CONDITION_NOISY_NEIGHBOUR_OPERATION_KEY = "WEB_FRAGMENT_CONDITION";
    private static final String CONDITION_JMX_METRIC_KEY_MATCHER = "category00=web,category01=fragment,name=condition";

    @Test
    public void whenConditionsAreTriggered_thenJmxBeansShouldIncludeTheTimeTakenForConditionShouldDisplayLogic() {
        startConditionsSimulationOperation();

        final List<String> mBeanNames = getAllEmittedJmxBeans();
        List<String> emittedConditionBeans = getWebConditionsBeanNames(mBeanNames);

        assertThat(emittedConditionBeans)
                .describedAs("Condition metrics bean have been emitted to JMX")
                .hasSize(1);

        emittedConditionBeans.forEach(bean -> {
            Response jmxDetails = getDetailedJmxEntry(bean);

            assertThat(getBeanCallCount(jmxDetails))
                    .describedAs("Condition calls are counted correctly")
                    .isGreaterThanOrEqualTo(1);

            assertThat(getBeanAverageTime(jmxDetails))
                    .describedAs("Condition execution timed correctly")
                    .isBetween(600d, 1000d);
        });
    }

    private int getBeanCallCount(Response jmxDetails) {
        return Integer.parseInt(jmxDetails.jsonPath().get("Count"));
    }

    private Double getBeanAverageTime(Response jmxDetails) {
        return Double.parseDouble(jmxDetails.jsonPath().get("Mean"));
    }

    private Response getDetailedJmxEntry(String bean) {
        return adminJsonRequest()
                .then()
                .log()
                .ifValidationFails()
                .statusCode(200)
                .contentType(JSON)
                .when()
                .get(CommonRestTestConstants.getNoisyNeighbourUrl("/jmx/" + bean));
    }

    private List<String> getAllEmittedJmxBeans() {
        return Arrays.asList(adminJsonRequest()
                .then()
                .log()
                .ifValidationFails()
                .statusCode(200)
                .contentType(JSON)
                .when()
                .get(CommonRestTestConstants.getNoisyNeighbourUrl("/jmx"))
                .as(String[].class));
    }

    private void startConditionsSimulationOperation() {
        String operationsResourceUrl = CommonRestTestConstants.getNoisyNeighbourUrl("/admin/block");

        adminJsonRequest()
                .given()
                .body(CONDITION_NOISY_NEIGHBOUR_OPERATION_KEY)
                .contentType(JSON)
                .when()
                .post(operationsResourceUrl)
                .then()
                .log()
                .ifValidationFails()
                .statusCode(OK.getStatusCode());
    }

    private RequestSpecification adminJsonRequest() {
        return RestAssured.given()
                .auth()
                .preemptive()
                .basic(CommonRestTestConstants.getAdminUser(), CommonRestTestConstants.getAdminPassword())
                .contentType(JSON);
    }

    @Nonnull
    private List<String> getWebConditionsBeanNames(List<String> mBeanNames) {
        return mBeanNames.stream()
                // Match on the transform task metric
                .filter(n -> n.contains(CONDITION_JMX_METRIC_KEY_MATCHER))
                // Match it's from the noisy neighbour plugin' plugin
                .filter(n -> n.contains("com.atlassian.diagnostics.noisy-neighbour-plugin"))
                .collect(Collectors.toList());
    }
}
