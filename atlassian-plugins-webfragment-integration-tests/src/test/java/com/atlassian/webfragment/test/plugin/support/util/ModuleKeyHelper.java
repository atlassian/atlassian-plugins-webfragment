package com.atlassian.webfragment.test.plugin.support.util;

import org.osgi.framework.BundleContext;

import com.atlassian.plugin.ModuleCompleteKey;
import com.atlassian.plugin.osgi.util.OsgiHeaderUtil;

public class ModuleKeyHelper {
    private final BundleContext bundleContext;

    public ModuleKeyHelper(BundleContext bundleContext) {
        this.bundleContext = bundleContext;
    }

    public String buildCompleteKeyForThisPlugin(String moduleKey) {
        var bundle = bundleContext.getBundle();
        var pluginKey = OsgiHeaderUtil.getPluginKey(bundle);
        var completeModuleKey = new ModuleCompleteKey(pluginKey, moduleKey);
        return completeModuleKey.getCompleteKey();
    }
}
