package com.atlassian.webfragment.test.plugin.spring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.osgi.framework.BundleContext;

import com.atlassian.plugin.web.api.DynamicWebInterfaceManager;
import com.atlassian.webfragment.test.plugin.support.util.ModuleKeyHelper;

import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.importOsgiService;

@Configuration
public class WiredTestBeans {

    // ----------------- OSGI SERVICE IMPORTS -----------------

    @Bean
    public DynamicWebInterfaceManager dynamicWebInterfaceManager() {
        return importOsgiService(DynamicWebInterfaceManager.class);
    }

    // ----------------- LOCAL BEANS -----------------

    @Bean
    public ModuleKeyHelper moduleKeyHelper(BundleContext bundleContext) {
        return new ModuleKeyHelper(bundleContext);
    }
}
