package com.atlassian.webfragment.test.plugin.support.feature.webitem.provider;

import java.util.Map;
import javax.annotation.Nonnull;

import com.atlassian.plugin.web.api.WebItem;
import com.atlassian.plugin.web.api.descriptors.WebItemProviderModuleDescriptor;
import com.atlassian.plugin.web.api.provider.WebItemProvider;

import static java.util.Collections.singleton;

public class ParamPassthroughWebItemProvider implements WebItemProvider {
    private WebItemProviderModuleDescriptor<WebItemProvider> moduleDescriptor;

    @Override
    public void init(@Nonnull final WebItemProviderModuleDescriptor<WebItemProvider> moduleDescriptor) {
        WebItemProvider.super.init(moduleDescriptor);

        this.moduleDescriptor = moduleDescriptor;
    }

    @Override
    public Iterable<WebItem> getItems(Map<String, Object> context) {
        return singleton(new WebItem() {
            @Nonnull
            @Override
            public String getSection() {
                return moduleDescriptor.getSection();
            }

            @Nonnull
            @Override
            public String getUrl() {
                return "";
            }

            @Override
            public String getAccessKey() {
                return "";
            }

            @Override
            public String getEntryPoint() {
                return "";
            }

            @Override
            public String getCompleteKey() {
                return "";
            }

            @Override
            public String getLabel() {
                return "";
            }

            @Override
            public String getTitle() {
                return "";
            }

            @Override
            public String getStyleClass() {
                return "";
            }

            @Override
            public String getId() {
                return "";
            }

            @Nonnull
            @Override
            public Map<String, String> getParams() {
                return moduleDescriptor.getParams();
            }

            @Override
            public int getWeight() {
                return 0;
            }
        });
    }
}
