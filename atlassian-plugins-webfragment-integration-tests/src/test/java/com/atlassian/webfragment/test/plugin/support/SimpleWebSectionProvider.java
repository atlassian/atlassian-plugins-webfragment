package com.atlassian.webfragment.test.plugin.support;

import java.util.Map;
import javax.annotation.Nonnull;

import com.atlassian.plugin.web.api.WebSection;
import com.atlassian.plugin.web.api.provider.WebSectionProvider;
import com.atlassian.webfragment.test.plugin.support.util.ModuleKeyHelper;

import static java.util.Collections.emptyMap;
import static java.util.Collections.singleton;

public class SimpleWebSectionProvider implements WebSectionProvider {

    private final ModuleKeyHelper moduleKeyHelper;

    public SimpleWebSectionProvider(ModuleKeyHelper moduleKeyHelper) {
        this.moduleKeyHelper = moduleKeyHelper;
    }

    @Override
    public Iterable<WebSection> getSections(Map<String, Object> context) {
        return singleton(new WebSection() {
            @Nonnull
            @Override
            public String getLocation() {
                return "web-section-provided";
            }

            @Override
            public String getCompleteKey() {
                return moduleKeyHelper.buildCompleteKeyForThisPlugin(getId());
            }

            @Override
            public String getLabel() {
                return "Provided Web-Section for testing";
            }

            @Override
            public String getTitle() {
                return "";
            }

            @Override
            public String getStyleClass() {
                return "";
            }

            @Override
            public String getId() {
                return "web-section-provided";
            }

            @Nonnull
            @Override
            public Map<String, String> getParams() {
                return emptyMap();
            }

            @Override
            public int getWeight() {
                return 1;
            }
        });
    }
}
