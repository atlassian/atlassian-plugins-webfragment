package com.atlassian.webfragment.test.plugin.support.feature.websection.provider;

import java.util.Map;
import javax.annotation.Nonnull;

import com.atlassian.plugin.web.api.WebSection;
import com.atlassian.plugin.web.api.descriptors.WebSectionProviderModuleDescriptor;
import com.atlassian.plugin.web.api.provider.WebSectionProvider;

import static java.util.Collections.singleton;

public class ParamPassthroughWebSectionProvider implements WebSectionProvider {
    private WebSectionProviderModuleDescriptor<WebSectionProvider> moduleDescriptor;

    @Override
    public void init(@Nonnull final WebSectionProviderModuleDescriptor<WebSectionProvider> moduleDescriptor) {
        WebSectionProvider.super.init(moduleDescriptor);

        this.moduleDescriptor = moduleDescriptor;
    }

    @Override
    public Iterable<WebSection> getSections(Map<String, Object> context) {
        return singleton(new WebSection() {
            @Nonnull
            @Override
            public String getLocation() {
                return moduleDescriptor.getLocation();
            }

            @Override
            public String getCompleteKey() {
                return "";
            }

            @Override
            public String getLabel() {
                return "";
            }

            @Override
            public String getTitle() {
                return "";
            }

            @Override
            public String getStyleClass() {
                return "";
            }

            @Override
            public String getId() {
                return "";
            }

            @Nonnull
            @Override
            public Map<String, String> getParams() {
                return moduleDescriptor.getParams();
            }

            @Override
            public int getWeight() {
                return 0;
            }
        });
    }
}
