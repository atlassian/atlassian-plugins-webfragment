package com.atlassian.webfragment.test.plugin.support;

import java.util.Map;
import javax.annotation.Nonnull;

import com.atlassian.plugin.web.api.WebItem;
import com.atlassian.plugin.web.api.provider.WebItemProvider;
import com.atlassian.webfragment.test.plugin.support.util.ModuleKeyHelper;

import static java.util.Collections.emptyMap;
import static java.util.Collections.singleton;

public class SimpleWebItemProvider implements WebItemProvider {

    private final ModuleKeyHelper moduleKeyHelper;

    public SimpleWebItemProvider(ModuleKeyHelper moduleKeyHelper) {
        this.moduleKeyHelper = moduleKeyHelper;
    }

    @Override
    public Iterable<WebItem> getItems(Map<String, Object> context) {
        return singleton(new WebItem() {
            @Nonnull
            @Override
            public String getSection() {
                return "system.admin/general";
            }

            @Nonnull
            @Override
            public String getUrl() {
                return "admin";
            }

            @Override
            public String getAccessKey() {
                return "";
            }

            @Override
            public String getEntryPoint() {
                return "";
            }

            @Override
            public String getCompleteKey() {
                return moduleKeyHelper.buildCompleteKeyForThisPlugin(getId());
            }

            @Override
            public String getLabel() {
                return "Provided Web-Item for testing";
            }

            @Override
            public String getTitle() {
                return "";
            }

            @Override
            public String getStyleClass() {
                return "";
            }

            @Override
            public String getId() {
                return "web-item-provided";
            }

            @Nonnull
            @Override
            public Map<String, String> getParams() {
                return emptyMap();
            }

            @Override
            public int getWeight() {
                return 1;
            }
        });
    }
}
