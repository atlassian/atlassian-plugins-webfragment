1. Ensure `com.atlassian.plugin.web.api` and all sub-packages are public
2. Update all usages of `com.atlassian.plugin.web.baseconditions` to `com.atlassian.plugin.web.api.baseconditions`
3. Update all usages of `com.atlassian.plugin.web.renderer` to `com.atlassian.plugin.web.api.renderer`
4. Update all usages of `com.atlassian.plugin.web.descriptors` to `com.atlassian.plugin.web.api.descriptors`
5. Products migrate to `com.atlassian.plugin.web.impl.DefaultWebInterfaceManager`, plugins use the
   interface `DynamicWebInterfaceManager`
6. Remove usages of the `com.atlassian.plugin.web.conditions` helpers and migrate to
   `com.atlassian.plugin.web.api.conditions.ConditionLoadingException`
