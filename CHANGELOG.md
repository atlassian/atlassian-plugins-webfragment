# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [7.2.0]

### Added

- [PLUGFRAG-46] Optional #init methods to WebItemProvider and WebSectionProvider to get parameters and other basic info

### Changed

- [PLUGFRAG-19] Allow implementers to Condition#init

### Fixed

- Log message saying "WebItemProvider" instead of "WebSectionProvider"

## [7.1.1] - 2024-08-21

### Fixed

- [PLUGFRAG-47] Context creation of WebPanels now happens once

## [7.1.0]

### Added

- [DCA11Y-1177] Add HTML traceability comments for web-panels

## [7.0.1]

### Fixed

- [CONFSRVDEV-32450] Fix DefaultWebPanelModuleDescriptor not supporting the new WebPanel interface

## [7.0.0]

### Fixed

- [PLUGFRAG-34] Split packages so we can use the DMZ
- [DCPL-811] Adopt new Atlassian Plugins in major version of Atlassian Webfragment

## [6.1.0-m01]

### Changed

* Migrate to the `6.5.0-m02` plugin version.

## [6.0.1]

### Changed

* Upgrade the spring scanner version to 2.2.5
* [ITAS-90] Replaced Condition#shouldDisplay timing event with JMX metric

## [5.3.2]

### Changed

* [SPFE-498] Revert the reverting changes to collections and instead
revert the comparator to remove the integer under/over-flow bug

## [5.3.1]

### Changed

* UP-MERGE: Make the Condition evaluation time event asynchronous preferred
* [SPFE-498] Revert changes to collections (thus restoring lazy
  behaviour)

## [5.3.0]

### Changed

* Implementation details to improve performance

## [5.2.3]

### Changed

* Make the Condition evaluation time event asynchronous preferred

## [5.2.2]

### Changed

* Only fire condition evaluation time events if greater than 5ms

## [5.2.1]

### Changed

* Release properly (5.2.0 was not)

## [5.2.0]

### Added

* Analytic events for evaluation of web fragment conditions. Read the upgrade guide to learn more

## [5.0.0]

### Changed

* Source and target Java version is now Java 8 (previously 7)
* Adds support for running Java 11
* Now requires the host product export Platform 5

[DCA11Y-1177]: https://hello.jira.atlassian.cloud/browse/DCA11Y-1177

[DCPL-811]: https://ecosystem.atlassian.net/browse/DCPL-811

[ITAS-90]: https://bulldog.internal.atlassian.com/browse/ITAS-90

[PLUGFRAG-19]: https://ecosystem.atlassian.net/browse/PLUGFRAG-19
[PLUGFRAG-34]: https://ecosystem.atlassian.net/browse/PLUGFRAG-34
[PLUGFRAG-46]: https://ecosystem.atlassian.net/browse/PLUGFRAG-46
[PLUGFRAG-47]: https://ecosystem.atlassian.net/browse/PLUGFRAG-47

[SPFE-498]: https://ecosystem.atlassian.net/browse/SPFE-498
