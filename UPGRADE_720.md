# For product and platform teams

## 1. Adding support for `WebItemProviderModuleDescriptor#getSection` and
`WebSectionProviderModuleDescriptor#getLocation`

These methods need to be implemented, if you've extended/overridden these, then you'll need to ensure this happens.

## 2. Adding support for `WebItemProvider#init` and `WebSectionProvider#init`

These methods need to be called by the `WebItemProviderModuleDescriptor` and `WebSectionProviderModuleDescriptor`, if
you've extended/overridden these, then you'll need to ensure this happens.  
